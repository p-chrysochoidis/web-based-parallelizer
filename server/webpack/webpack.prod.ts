import webpack from "webpack";
import merge from "webpack-merge";
import commonWebpack from "./webpack.common";

const prodConfig: webpack.Configuration = {
    mode: "production"
};

const fullProdConfig: webpack.Configuration = merge.smart(commonWebpack, prodConfig);

export default fullProdConfig;