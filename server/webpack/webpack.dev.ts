import webpack from "webpack";
import merge from "webpack-merge";
import commonWebpack from "./webpack.common";

const devConfig: webpack.Configuration = {
    mode: "development",
    devtool: "eval"
}

const fullDevConfig: webpack.Configuration = merge.smart(commonWebpack, devConfig);

export default fullDevConfig;