import webpack from "webpack";
import path from "path";
import CleanWebpackPlugin from "clean-webpack-plugin";

const config: webpack.Configuration = {
    entry: {
        index: root("src/index")
    },
    output: {
        path: root("dist"),
        filename: "[name].js",
        libraryTarget: 'commonjs'
    },
    node: {
        __dirname: false
    },
    module: {
        rules: [
          {
            test: /\.tsx?$/,
            use: {
                loader: 'ts-loader'
            },
            exclude: /node_modules/
          }
        ]
    },
    externals: [
        'express',
        'socket\.io',
        'rxjs',
        'rxjs/operators'
    ],
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ]
    },
    plugins: [
        new CleanWebpackPlugin(
            [root("dist")],
            {
                root: root()
            }
        )
    ],
    target: "node"
};

function root(...pathToFile: string[]): string {
    return path.join(__dirname, "../", ...pathToFile);
}

export default config;