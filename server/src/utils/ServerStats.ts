import { TimeCounter } from "./TimeCounter";
import { Logger } from "./log/Logger";
import { TimedValue } from "./TimedValue";

export class ServerStats {
    private mLogger: Logger = Logger.gi();
    private mTotalDuration: TimeCounter = new TimeCounter();
    private mIsAllDone: boolean = false;
    private mWorkGenerationStarted: boolean = false;
    private mAllWorkGenerationCompleted: boolean = false;
    private mWorkGenerationAllDurationTimer: TimeCounter = new TimeCounter();
    private mWorkGenerationDurations: TimeCounter[] = [];
    private mGeneratedWorkFeedDurations: { batches: TimeCounter[], timer: TimeCounter }[] = [];
    private mIsWorkProcessingStarted: boolean = false;
    private mIsWorkProcessingCompleted: boolean = false;
    private mWorkProcessingDuration: TimeCounter = new TimeCounter();
    private mWorkCirclesProcessingDurations: TimeCounter[] = [];
    private mWorkProcessingDurations: Map<string, TimeCounter[]> = new Map();
    private mWorkProcessedByWorker: Map<string, { processing: number, completed: number, failed: number }> = new Map();
    private mWorkProcessedByWorkerInTime: Map<string, { processing: TimedValue<number>[], completed: TimedValue<number>[], failed: TimedValue<number>[] }> = new Map();
    private mWorkLatencyByWorker: Map<string, {total: number, average: number, min: number, max: number}> = new Map(); 
    private mWorkLatencyByWorkerInTime: Map<string, {total: TimedValue<number>[], average: TimedValue<number>[], min: TimedValue<number>[], max: TimedValue<number>[]}> = new Map();
    private mResultHandlingDuration: TimeCounter = new TimeCounter();
    private mWorkersAndWorkStats: {
        workers: {
            totalConnected: number,
            totalProcesses: number,
            available: number,
            availableProcesses: number
        },
        work: {
            totalCompleted: number,
            totalInProgress: number,
            totalFailed: number,
            totalWaitingProcessing: number,
            duration: {
                total: number,
                average: number,
                min: number,
                max: number,
            },
            clientDuration: {
                total: number,
                average: number,
                min: number,
                max: number,
            },
            latency: { // it includes the tranfer of data time.
                total: number,
                average: number,
                min: number,
                max: number,
            }
        }
    } = {
        workers: {
            totalConnected: 0,
            totalProcesses: 0,
            available: 0,
            availableProcesses: 0
        },
        work: {
            totalCompleted: 0,
            totalInProgress: 0,
            totalFailed: 0,
            totalWaitingProcessing: 0,
            duration: {
                total: 0,
                average: 0,
                min: null,
                max: 0,
            },
            clientDuration: {
                total: 0,
                average: 0,
                min: null,
                max: 0,
            },
            latency: {
                total: 0,
                average: 0,
                min: null,
                max: 0,
            }
        }
    }

    constructor() {
    }

    public allWorkGenerationCompleted() {
        this.mLogger.debug(`[ServerStats] allWorkGenerationCompleted`);
        this.mAllWorkGenerationCompleted = true;
        this.mWorkGenerationAllDurationTimer.stop();
    }

    public allWorkProcessingCompleted() {
        this.mLogger.debug(`[ServerStats] allWorkProcessingCompleted`);
        this.mIsWorkProcessingCompleted = true;
        this.mWorkProcessingDuration.stop();
    }

    /**
     * Because every work circle is sequential this
     * function asumes that the last one started just
     * completed.
     */
    public generatedWorkFeedCompleted() {
        this.mLogger.debug(`[ServerStats] generatedWorkFeedCompleted`);
        const length = this.mGeneratedWorkFeedDurations.length;
        if (!length) {
            return;
        }
        this.mLogger.debug(`[ServerStats] generatedWorkFeedCompleted circle ${length - 1}`);
        this.mGeneratedWorkFeedDurations[length - 1].timer.stop();
    }

    public generatedWorkFeedStarted() {
        this.mLogger.debug(`[ServerStats] generatedWorkFeedStarted circle ${this.mGeneratedWorkFeedDurations.length}`);
        this.mGeneratedWorkFeedDurations.push({ batches: [], timer: new TimeCounter(true) });
    }


    public generatedWorkFeedBatchCompleted() {
        this.mLogger.debug(`[ServerStats] generatedWorkFeedBatchCompleted`);
        const feedLength = this.mGeneratedWorkFeedDurations.length;
        if (!feedLength) {
            return;
        }
        this.mLogger.debug(`[ServerStats] generatedWorkFeedBatchCompleted circle ${feedLength - 1}`);
        const feed = this.mGeneratedWorkFeedDurations[feedLength - 1]
        const batchesLength = feed.batches.length;
        if (!batchesLength) {
            return;
        }
        this.mLogger.debug(`[ServerStats] generatedWorkFeedBatchCompleted batch ${batchesLength - 1}`);
        feed.batches[batchesLength - 1].stop();
    }

    public generatedWorkFeedBatchStarted() {
        this.mLogger.debug(`[ServerStats] generatedWorkFeedBatchStarted`);
        const feedLength = this.mGeneratedWorkFeedDurations.length;
        if (!feedLength) {
            return;
        }
        const feed = this.mGeneratedWorkFeedDurations[feedLength - 1];
        this.mLogger.debug(`[ServerStats] generatedWorkFeedBatchStarted circle ${feedLength - 1} batch ${feed.batches.length}`);
        feed.batches.push(new TimeCounter(true));
    }

    public processingCompletedForWork(workId: string, workerId: string, clientProcesstime: number) {
        this.mLogger.debug(`[ServerStats] processingCompletedForWork workId: ${workId} workerId: ${workerId} clientProcesstime: ${clientProcesstime}`);
        if (!this.mWorkProcessingDurations.has(workId) || !this.mWorkProcessedByWorker.has(workerId) || !this.mWorkProcessedByWorkerInTime.has(workerId)) {
            this.mLogger.debug(`[ServerStats] processingCompletedForWork workId: ${workId} workerId: ${workerId} not found in maps`);
            return;
        }
        let workDurations = this.mWorkProcessingDurations.get(workId);
        if (!workDurations.length) {
            this.mLogger.debug(`[ServerStats] processingCompletedForWork no work duration found`);
            return;
        }
        if (!this.mWorkLatencyByWorker.has(workerId)) {
            this.mWorkLatencyByWorker.set(workerId, {total: 0, average: 0, min: null, max: 0});
        }
        if (!this.mWorkLatencyByWorkerInTime.has(workerId)) {
            this.mWorkLatencyByWorkerInTime.set(workerId, {total: [], average: [], min: [], max: []});
        }
        let workTimer = workDurations[workDurations.length - 1];
        let currentWorkDuration = workTimer.stop();
        workTimer.otherSourceDuration = clientProcesstime;
        let workerStats = this.mWorkProcessedByWorker.get(workerId);
        workerStats.processing--;
        workerStats.completed++;
        let workerStatsInTime = this.mWorkProcessedByWorkerInTime.get(workerId);
        workerStatsInTime.processing.push(new TimedValue(workerStats.processing));
        workerStatsInTime.completed.push(new TimedValue(workerStats.completed));
        this.mWorkersAndWorkStats.work.totalCompleted++;
        this.mWorkersAndWorkStats.work.totalInProgress--;

        this.mWorkersAndWorkStats.work.duration.total += currentWorkDuration;
        this.mWorkersAndWorkStats.work.duration.average = this.mWorkersAndWorkStats.work.duration.total / this.mWorkersAndWorkStats.work.totalCompleted;
        this.mWorkersAndWorkStats.work.duration.min = this.mWorkersAndWorkStats.work.duration.min === null ? currentWorkDuration : Math.min(this.mWorkersAndWorkStats.work.duration.min, currentWorkDuration);
        this.mWorkersAndWorkStats.work.duration.max = Math.max(this.mWorkersAndWorkStats.work.duration.max, currentWorkDuration);

        this.mWorkersAndWorkStats.work.clientDuration.total += clientProcesstime;
        this.mWorkersAndWorkStats.work.clientDuration.average = this.mWorkersAndWorkStats.work.clientDuration.total / this.mWorkersAndWorkStats.work.totalCompleted;
        this.mWorkersAndWorkStats.work.clientDuration.min = this.mWorkersAndWorkStats.work.clientDuration.min === null ? clientProcesstime : Math.min(this.mWorkersAndWorkStats.work.clientDuration.min, clientProcesstime);
        this.mWorkersAndWorkStats.work.clientDuration.max = Math.max(this.mWorkersAndWorkStats.work.clientDuration.max, clientProcesstime);

        let latency = workTimer.durationsDiff;
        this.mWorkersAndWorkStats.work.latency.total += latency;
        this.mWorkersAndWorkStats.work.latency.average = this.mWorkersAndWorkStats.work.latency.total / this.mWorkersAndWorkStats.work.totalCompleted;
        this.mWorkersAndWorkStats.work.latency.min = this.mWorkersAndWorkStats.work.latency.min === null ? latency : Math.min(this.mWorkersAndWorkStats.work.latency.min, latency);
        this.mWorkersAndWorkStats.work.latency.max = Math.max(this.mWorkersAndWorkStats.work.latency.max, latency);

        let totalWorkerLatencyNumbers = this.mWorkLatencyByWorker.get(workerId);
        totalWorkerLatencyNumbers.total += latency;
        totalWorkerLatencyNumbers.average = totalWorkerLatencyNumbers.total / workerStats.completed;
        totalWorkerLatencyNumbers.min = totalWorkerLatencyNumbers.min === null ? latency : Math.min(totalWorkerLatencyNumbers.min, latency);
        totalWorkerLatencyNumbers.max = Math.max(totalWorkerLatencyNumbers.max, latency);

        let totalWorkerLatencyNumbersInTime = this.mWorkLatencyByWorkerInTime.get(workerId);
        totalWorkerLatencyNumbersInTime.total.push(new TimedValue(totalWorkerLatencyNumbers.total));
        totalWorkerLatencyNumbersInTime.average.push(new TimedValue(totalWorkerLatencyNumbers.average));
        totalWorkerLatencyNumbersInTime.min.push(new TimedValue(totalWorkerLatencyNumbers.min));
        totalWorkerLatencyNumbersInTime.max.push(new TimedValue(totalWorkerLatencyNumbers.max));

    }

    public processingFailedForWork(workId: string, workerId: string) {
        this.mLogger.debug(`[ServerStats] processingFailedForWork workId: ${workId} workerId: ${workerId}`);
        if (!this.mWorkProcessingDurations.has(workId) || !this.mWorkProcessedByWorker.has(workerId) || !this.mWorkProcessedByWorkerInTime.has(workerId)) {
            this.mLogger.debug(`[ServerStats] processingFailedForWork workId: ${workId} workerId: ${workerId} not found in maps`);
            return;
        }
        let workerStats = this.mWorkProcessedByWorker.get(workerId);
        workerStats.processing--;
        workerStats.failed++;
        let workerStatsInTime = this.mWorkProcessedByWorkerInTime.get(workerId);
        workerStatsInTime.processing.push(new TimedValue(workerStats.processing));
        workerStatsInTime.failed.push(new TimedValue(workerStats.failed));
        this.mWorkersAndWorkStats.work.totalFailed++;
        this.mWorkersAndWorkStats.work.totalInProgress--;
    }

    public processingStartedForWork(workId: string, workerId: string) {
        this.mLogger.debug(`[ServerStats] processingStartedForWork workId: ${workId} workerId: ${workerId}`);
        this.initInMapIfNotThere(this.mWorkProcessingDurations, workId, [])
        let workDurations = this.mWorkProcessingDurations.get(workId);
        workDurations.push(new TimeCounter(true));
        this.initInMapIfNotThere(this.mWorkProcessedByWorker, workerId, { processing: 0, completed: 0, failed: 0 });
        let workerStats = this.mWorkProcessedByWorker.get(workerId);
        workerStats.processing++;
        this.initInMapIfNotThere(this.mWorkProcessedByWorkerInTime, workerId, { processing: [], completed: [], failed: [] });
        let workerStatsInTime = this.mWorkProcessedByWorkerInTime.get(workerId);
        workerStatsInTime.processing.push(new TimedValue(workerStats.processing));
        this.mWorkersAndWorkStats.work.totalInProgress++;
    }

    public resultHandlingCompleted() {
        this.mLogger.debug(`[ServerStats] resultHandlingCompleted`);
        this.mTotalDuration.stop();
        this.mResultHandlingDuration.stop();
        this.mIsAllDone = true;
    }

    public resultHandlingStarted() {
        this.mLogger.debug(`[ServerStats] resultHandlingStarted`);
        this.mResultHandlingDuration.start();
    }

    public totalAvailableWorkChanged(availableWork: number) {
        this.mWorkersAndWorkStats.work.totalWaitingProcessing = availableWork;
    }

    public totalAvailableWorkerProcessesChanged(availableWorkerProcesses: number) {
        this.mWorkersAndWorkStats.workers.availableProcesses = availableWorkerProcesses;
    }

    public totalAvailableWorkersChanged(availableWorkers: number) {
        this.mWorkersAndWorkStats.workers.available = availableWorkers;
    }

    public totalWorkersChanged(connected: boolean, numberOfProcesses: number) {
        if (connected) {
            this.mWorkersAndWorkStats.workers.totalConnected++;
            this.mWorkersAndWorkStats.workers.totalProcesses += numberOfProcesses;
        } else {
            this.mWorkersAndWorkStats.workers.totalConnected--;
            this.mWorkersAndWorkStats.workers.totalProcesses -= numberOfProcesses;
        }
    }
    public workGenerationStarted() {
        if (!this.mWorkGenerationStarted) {
            this.mLogger.debug(`[ServerStats] workGenerationStarted`);
            this.mWorkGenerationStarted = true;
            this.mWorkGenerationAllDurationTimer.start();
            this.mTotalDuration.start();
        }
        this.mLogger.debug(`[ServerStats] workGenerationStarted work circle ${this.mWorkGenerationDurations.length}`);
        this.mWorkGenerationDurations.push(new TimeCounter(true));
    }

    /**
     * Because every work circle is sequential this
     * function asumes that the last one started just
     * completed.
     */
    public workGenerationCompleted() {
        this.mLogger.debug(`[ServerStats] workGenerationCompleted`);
        const length = this.mWorkGenerationDurations.length;
        if (!length) {
            return;
        }
        this.mLogger.debug(`[ServerStats] workGenerationCompleted work circle ${length - 1}`);
        this.mWorkGenerationDurations[length - 1].stop();
    }

    public worksProcessingStarted() {
        if (!this.mIsWorkProcessingStarted) {
            this.mLogger.debug(`[ServerStats] workProcessingStarted`);
            this.mIsWorkProcessingStarted = true;
            this.mWorkProcessingDuration.start();
        }
        this.mLogger.debug(`[ServerStats] workProcessingStarted work circle ${this.mWorkCirclesProcessingDurations.length}`);
        this.mWorkCirclesProcessingDurations.push(new TimeCounter(true));
    }

    public worksProcessingCompleted() {
        this.mLogger.debug(`[ServerStats] worksProcessingCompleted`);
        const length = this.mWorkCirclesProcessingDurations.length;
        if (!length) {
            return;
        }
        this.mLogger.debug(`[ServerStats] worksProcessingCompleted work circle ${length - 1}`);
        this.mWorkCirclesProcessingDurations[length - 1].stop();
    }

    public toJsonData(): any {
        return {
            workGenerationStarted: this.mWorkGenerationStarted,
            workGenerationCompleted: this.mAllWorkGenerationCompleted,
            workProcessingStarted: this.mIsWorkProcessingStarted,
            workProcessingCompleted: this.mIsWorkProcessingCompleted,
            allDone: this.mIsAllDone,
            totalDuration: this.mTotalDuration.toJson(),
            workGenerationDuration: this.mWorkGenerationAllDurationTimer.toJson(),
            workProcessingDuration: this.mWorkProcessingDuration.toJson(),
            resultHandlingDuration: this.mResultHandlingDuration.toJson(),
            workAndWorkersTotals: this.mWorkersAndWorkStats,
            workGenerationsCirclesDurations: this.mWorkGenerationDurations.map((timer) => timer.toJson()),
            generatedWorkFeedCirclesDurations: this.mGeneratedWorkFeedDurations.map((feedData) => {
                return {
                    wholeCircle: feedData.timer.toJson(),
                    batches: feedData.batches.map((timer) => timer.toJson())
                };
            }),
            workCircleProcessingDurations: this.mWorkCirclesProcessingDurations.map((timer) => timer.toJson()),
            individualWorkProcessingDurations: this.getIndividualWorkProcessingDurations(),
            individualWorkProcessingTotalsPerWorker: this.getIndividualWorkProcessingTotalsPerWorker(),
            individualWorkProcessingTotalsPerWorkerInTime: this.getIndividualWorkProcessingTotalsPerWorkerInTime(),
            latencyByWorker: this.getLatencyByWorker(),
            latencyByWorkerInTime: this.getLatencyByWorkerInTime(),
        };
    }

    private getIndividualWorkProcessingDurations() {
        return this.doMapOfMap(this.mWorkProcessingDurations, (timers, workId) => {
            let currentDuration = timers.find((timer) => !!timer.endedAt);
            return {
                workId: workId,
                duration: currentDuration && currentDuration.toJson()
            };
        });
    }

    private getIndividualWorkProcessingTotalsPerWorker() {
        return this.doMapOfMap(this.mWorkProcessedByWorker, (totals, workerId) => {
            return {
                workerId: workerId,
                totals: {
                    processing: totals.processing,
                    completed: totals.completed,
                    failed: totals.failed
                }
            };
        });
    }

    private getIndividualWorkProcessingTotalsPerWorkerInTime() {
        return this.doMapOfMap(this.mWorkProcessedByWorkerInTime, (totals, workerId) => {
            return {
                workerId: workerId,
                totals: {
                    processing: totals.processing.map((t) => t.toJson()),
                    completed: totals.completed.map((t) => t.toJson()),
                    failed: totals.failed.map((t) => t.toJson())
                }
            };
        });
    }

    private getLatencyByWorker() {
        return this.doMapOfMap(this.mWorkLatencyByWorker, (totals, workerId) => {
            return {
                workerId: workerId,
                totals: totals
            };
        });
    }

    private getLatencyByWorkerInTime() {
        return this.doMapOfMap(this.mWorkLatencyByWorkerInTime, (totals, workerId) => {
            return {
                workerId: workerId,
                totals: {
                    total: totals.total.map((t) => t.toJson()),
                    average: totals.average.map((t) => t.toJson()),
                    min: totals.min.map((t) => t.toJson()),
                    max: totals.max.map((t) => t.toJson()),
                }
            };
        });
    }
    private initInMapIfNotThere<K, V>(map: Map<K, V>, key: K, value: V): Map<K, V> {
        if (!map.has(key)) {
            map.set(key, value);
        }
        return map;
    }

    private doMapOfMap<K, V, R>(map: Map<K, V>, callbackFn: (value: V, key: K) => R): R[] {
        let data = [];
        let entriesIt = map.entries();
        let entry;
        while (!(entry = entriesIt.next()).done) {
            data.push(callbackFn(entry.value[1], entry.value[0]));
        }
        return data;
    }
}