export class TimedValue<T> {
    private mDate: Date = new Date();
    private mValue: T;

    constructor(value: T) {
        this.mValue = value;
    }

    public toJson() {
        return {
            date: this.mDate.toUTCString(),
            value: this.mValue
        };
    }
}