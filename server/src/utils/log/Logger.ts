import { LogLevel } from "./LogLevel";

export class Logger {
    private static loggerInstance: Logger;

    private level: LogLevel;

    constructor() {
        this.level = this.getLogLevelFromEnvironment();
    }

    public static gi(): Logger {
        if (!Logger.loggerInstance) {
            Logger.loggerInstance = new Logger();
        }
        return Logger.loggerInstance;
    }

    public log(...logData: any[]) {
        this.logWithLevel(LogLevel.DEBUG, ...logData);
    }

    public debug(...logData: any[]) {
        this.logWithLevel(LogLevel.DEBUG, ...logData);
    }

    public error(...logData: any[]) {
        this.logWithLevel(LogLevel.ERROR, ...logData);
    }

    public fatal(...logData: any[]) {
        this.logWithLevel(LogLevel.FATAL, ...logData);
    }

    public info(...logData: any[]) {
        this.logWithLevel(LogLevel.INFO, ...logData);
    }

    public setLevel(level: LogLevel) {
        this.level = level;
    }

    public trace(...logData: any[]) {
        this.logWithLevel(LogLevel.TRACE, ...logData);
    }

    public warn(...logData: any[]) {
        this.logWithLevel(LogLevel.WARN, ...logData);
    }

    private getLogLevelFromEnvironment(): LogLevel {
        return process.env && process.env.NODE_ENV === "production" ? LogLevel.ERROR : LogLevel.ALL;
    }

    private logWithLevel(level: LogLevel, ...logData: any[]) {
        level >= this.level && console.log(...logData);
    }
}