export enum LogLevel {
    ALL, DEBUG, TRACE, INFO, WARN, ERROR, FATAL, OFF
}