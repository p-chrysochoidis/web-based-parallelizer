import { Logger } from "./log/Logger";

export class TimeCounter {
    private mStartedAt: Date;
    private mEndedAt: Date;
    private mDuration: number = -1;
    private mDurationFromOtherSource: number = -1;
    private mDurationsDiff: number = null;

    constructor(startNow: boolean = false) {
        if (startNow) {
            this.start();
        }
    }

    public start(): void {
        if (this.mStartedAt) {
            Logger.gi().debug(`[TimeCounter] Timer already started`);
            return;
        }
        this.mStartedAt = new Date();
    }

    public stop(): number {
        if (!this.mEndedAt) {
            this.mEndedAt = new Date();
            this.mDuration = this.calculateDuration();
            this.calculateDurationsDiff();
        }
        return this.mDuration;
    }

    public toJson(): any {
        let data: any = {
            startedAt: this.mStartedAt,
            endedAt: this.endedAt,
            duration: this.duration
        };

        if (this.mDurationFromOtherSource !== -1) {
            data['otherSourceDuration'] = this.mDurationFromOtherSource;
        }

        if (this.mDurationsDiff !== null) {
            data['durationsDiff'] = this.mDurationsDiff;
        }

        return data;
    }

    public get duration(): number {
        return this.mDuration;
    }

    public get endedAt(): Date {
        return this.mEndedAt;
    }

    public get durationsDiff(): number {
        return this.mDurationsDiff;
    }

    public get startedAt(): Date {
        return this.mStartedAt;
    }

    public set otherSourceDuration (duration: number) {
        this.mDurationFromOtherSource = duration;
        this.calculateDurationsDiff();
    }

    private calculateDuration(): number {
        if (!this.mStartedAt || !this.mEndedAt) {
            return -1;
        }
        return this.mEndedAt.getTime() - this.mStartedAt.getTime();
    }

    private calculateDurationsDiff() {
        if (this.mDuration !== -1 && this.mDurationFromOtherSource !== -1) {
            this.mDurationsDiff = this.mDuration - this.mDurationFromOtherSource;
        }
    }
}