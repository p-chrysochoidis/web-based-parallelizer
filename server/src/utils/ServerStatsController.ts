import { ServerStats } from "./ServerStats";
import { BehaviorSubject, Observable } from "rxjs";

export class ServerStatsController {
    private static instance: ServerStatsController = null;
    private mStats: BehaviorSubject<ServerStats>;

    public static gi() {
        if (ServerStatsController.instance === null) {
            ServerStatsController.instance = new ServerStatsController();
        }
        return ServerStatsController.instance;
    }
    
    constructor() {
        this.init();
    }

    public get stats (): Observable<ServerStats> {
        return this.mStats;
    }

    public allWorkGenerationCompleted() {
        this.mStats.value.allWorkGenerationCompleted();
        this.notifyStatsChanged();
    }

    public allWorkProcessingCompleted() {
        this.mStats.value.allWorkProcessingCompleted();
        this.notifyStatsChanged();
    }

    public generatedWorkFeedCompleted() {
        this.mStats.value.generatedWorkFeedCompleted();
        this.notifyStatsChanged();
    }

    public generatedWorkFeedStarted() {
        this.mStats.value.generatedWorkFeedStarted();
        this.notifyStatsChanged();
    }

    public generatedWorkFeedBatchCompleted() {
        this.mStats.value.generatedWorkFeedBatchCompleted();
        this.notifyStatsChanged();
    }

    public generatedWorkFeedBatchStarted() {
        this.mStats.value.generatedWorkFeedBatchStarted();
        this.notifyStatsChanged();
    }
    public processingCompletedForWork(workId: string, workerId: string, clientProcesstime: number) {
        this.mStats.value.processingCompletedForWork(workId, workerId, clientProcesstime);
        this.notifyStatsChanged();
    }

    public processingFailedForWork(workId: string, workerId: string) {
        this.mStats.value.processingFailedForWork(workId, workerId);
        this.notifyStatsChanged();
    }

    public processingStartedForWork(workId: string, workerId: string) {
        this.mStats.value.processingStartedForWork(workId, workerId);
        this.notifyStatsChanged();
    }

    public resultHandlingCompleted() {
        this.mStats.value.resultHandlingCompleted();
        this.notifyStatsChanged();
    }

    public resultHandlingStarted() {
        this.mStats.value.resultHandlingStarted();
        this.notifyStatsChanged();
    }

    public totalAvailableWorkChanged(availableWork: number) {
        this.mStats.value.totalAvailableWorkChanged(availableWork);
        this.notifyStatsChanged();
    }

    public totalAvailableWorkerProcessesChanged(availableWorkerProcesses: number) {
        this.mStats.value.totalAvailableWorkerProcessesChanged(availableWorkerProcesses);
        this.notifyStatsChanged();
    }
    public totalAvailableWorkersChanged(availableWorkers: number) {
        this.mStats.value.totalAvailableWorkersChanged(availableWorkers);
        this.notifyStatsChanged();
    }

    public totalWorkersChanged(connected: boolean, numberOfProcesses: number) {
        this.mStats.value.totalWorkersChanged(connected, numberOfProcesses);
        this.notifyStatsChanged();
    }

    public workGenerationStarted() {
        this.mStats.value.workGenerationStarted();
        this.notifyStatsChanged();
    }

    public workGenerationCompleted() {
        this.mStats.value.workGenerationCompleted();
        this.notifyStatsChanged();
    }

    public worksProcessingStarted() {
        this.mStats.value.worksProcessingStarted();
        this.notifyStatsChanged();
    }
    
    public worksProcessingCompleted() {
        this.mStats.value.worksProcessingCompleted();
        this.notifyStatsChanged();
    }
    private init() {
        this.mStats = new BehaviorSubject(new ServerStats());
    }

    private notifyStatsChanged() {
        this.mStats.next(this.mStats.value);
    }
}