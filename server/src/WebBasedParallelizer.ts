import { Server } from "./communications/Server";
import { WorkersController } from "./workers/WorkersController";
import path from 'path';
import { Logger } from "./utils/log/Logger";
import { WorkGenerator } from "./work/WorkGenerator";
import { WorkScheduler } from "./work/WorkScheduler";
import { WorkResultHandler } from "./work/WorkResultHandler";
import { MessagesExecutor } from "./communications/messages/MessagesExecutor";
import { WorkGenerationContract } from "./work/WorkGenerationContract";
import { WorkStorage } from "./work/WorkStorage";
import { ServerStatsController } from "./utils/ServerStatsController";

export class WebBasedParallelizer {
    private readonly staticFilesLocation: string = path.join(__dirname, "../../client/dist");
    private server: Server;
    private workersController: WorkersController;
    private workGenerator: WorkGenerator;
    private workScheduler: WorkScheduler;
    private mMessagesExecutor: MessagesExecutor;
    private mWorkStorage: WorkStorage;
    private mServerStatsController: ServerStatsController = ServerStatsController.gi();

    constructor(
        webWorkerCode: string,
        workGenerationContract: WorkGenerationContract,
        workResultHandler: WorkResultHandler,
        host: string = '0.0.0.0',
        port: number = 8080
    ) {
        this.server = new Server(this.staticFilesLocation, webWorkerCode, host, port);
        this.workersController = new WorkersController(this.server);
        this.mWorkStorage = new WorkStorage();
        this.workScheduler = new WorkScheduler(this.workersController, this.mWorkStorage);
        workResultHandler.setWorkScheduler(this.workScheduler);
        this.workGenerator = new WorkGenerator(this.workScheduler, workGenerationContract, this.mWorkStorage, workResultHandler);
        this.mMessagesExecutor = new MessagesExecutor(this.workersController, this.workGenerator, this.workScheduler, workResultHandler, this.mWorkStorage, this);
        this.updateStats();
    }

    public start() {
        this.server.start();
    }

    public startWork() {
        this.workGenerator.registerWork();
    }

    public stop() {
        //TODO: stop any ongoing work.
        this.server.stop();
        this.mMessagesExecutor.shutdown();
    }

    private updateStats() {
        this.workersController.numOfWorkersAvailable.subscribe((total) => {
            Logger.gi().debug(`[WebBasedParallelizer] Total workers available: ${total}`);
            this.mServerStatsController.totalAvailableWorkersChanged(total);
        });
        this.workersController.numOfWorkerProcessesAvailable.subscribe((n) => {
            Logger.gi().debug(`[WebBasedParallelizer]Total available processes: ${n}`);
            this.mServerStatsController.totalAvailableWorkerProcessesChanged(n);
        });
        this.workScheduler.totalAvailableWork.subscribe((n) => {
            Logger.gi().debug(`[WebBasedParallelizer]Total available work: ${n}`);
            this.mServerStatsController.totalAvailableWorkChanged(n);
        });
    }
}