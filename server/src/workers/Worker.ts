import { ClientConnection } from "../communications/ClientConnection";
import { Observable, BehaviorSubject, Subject, Subscription, merge } from "rxjs";
import { IncommingMessage } from "../communications/messages/incomming/IncommingMessage";
import { StartWorkMessage } from "../communications/messages/outgoing/StartWorkMessage";
import { WorkerStatus } from "./WorkerStatus";
import { Work } from "../work/Work";
import { WorkResult } from "../work/WorkResult";
import { Logger } from "../utils/log/Logger";
import { ServerStatsController } from "../utils/ServerStatsController";
import { auditTime, take, skip } from "rxjs/operators";
import { ServerStats } from "../utils/ServerStats";
import { ServerStatsUpdateMessage } from "../communications/messages/outgoing/ServerStatsUpdateMessage";
import { RegisterForServerStatsAnswerMessage } from "../communications/messages/outgoing/RegisterForServerStatsAnswerMessage";
import { RegisterForServerStatsMessage } from "../communications/messages/incomming/RegisterForServerStatsMessage";
import { OutgoingMessage } from "../communications/messages/outgoing/OutgoingMessage";

export class Worker {
    private readonly sendStatsOnePerMillisMax: number = 5 * 1000;
    private clientConnection: ClientConnection;
    private stateStream: BehaviorSubject<WorkerStatus> = new BehaviorSubject(WorkerStatus.notConnected);
    private numOfThreads: number;
    private threadsWorking: number = 0;
    private currentWork: Work[] = [];
    private logger: Logger = Logger.gi();
    private mFailedWorkStream: Subject<Work>;
    private mServerStatsController: ServerStatsController = ServerStatsController.gi();
    private mServerStatsSubscription: Subscription;

    constructor(clientConnection: ClientConnection, failedWorkStream: Subject<Work>) {
        this.clientConnection = clientConnection;
        this.stateStream.next(WorkerStatus.connected);
        this.mFailedWorkStream = failedWorkStream;
    }

    public get onMessage(): Observable<IncommingMessage> {
        return this.clientConnection.messages;
    }

    public get id(): string {
        return this.clientConnection.connectionId;
    }

    public workOn(work: Work): void {
        this.currentWork.push(work);
        let startWorkMessage = new StartWorkMessage(work);
        this.sendMessage(startWorkMessage);
        this.threadsWorking++;
        this.updateStatusForThreadChange();
    }

    public register(numOfThreads: number) {
        this.mServerStatsController.totalWorkersChanged(true, numOfThreads);
        this.numOfThreads = numOfThreads;
        this.stateStream.next(WorkerStatus.ready);
    }

    public registerForServerStats(incommingMessage: RegisterForServerStatsMessage) {
        this.sendMessage(new RegisterForServerStatsAnswerMessage(true, incommingMessage.id));
        if (this.mServerStatsSubscription) {
            this.mServerStatsSubscription.unsubscribe();
        }
        this.mServerStatsSubscription = merge(
            this.mServerStatsController.stats.pipe(take(1)),
            this.mServerStatsController.stats.pipe(
                skip(1),
                auditTime(this.sendStatsOnePerMillisMax)
            )
        )
            .subscribe((stats: ServerStats) => {
                this.logger.debug(`[${this.id}] sending server stats update`);
                const statsUpdateMessage = new ServerStatsUpdateMessage(stats);
                this.sendMessage(statsUpdateMessage);
            });
    }

    public sendMessage(message: OutgoingMessage) {
        this.clientConnection.sendMessage(message);
    }

    public workCompleted(workResult: WorkResult): void {
        this.logger.info(`[${this.id}] work completed.`);
        let workIndex = this.currentWork.findIndex((work: Work) => {
            return workResult.work.id === work.id;
        });
        if (workIndex === -1) {
            this.logger.debug(`[${this.id}] work not found in work list.`)
            return;
        }
        this.currentWork.splice(workIndex, 1);
        this.threadsWorking--;
        this.updateStatusForThreadChange();
    }

    public get availableProcesses(): number {
        return this.numOfThreads - this.threadsWorking;
    }

    public get state(): BehaviorSubject<WorkerStatus> {
        return this.stateStream;
    }

    public disconnected() {
        if (this.mServerStatsSubscription) {
            this.logger.debug(`[${this.id}] unsubscribe from server stats updates.`);
            this.mServerStatsSubscription.unsubscribe();
            this.mServerStatsSubscription = null;
        }
        this.clientConnection.close();
        if([WorkerStatus.connected, WorkerStatus.notConnected, WorkerStatus.disconnected].indexOf(this.state.getValue()) === -1) {
            this.mServerStatsController.totalWorkersChanged(false, this.numOfThreads);
        }
        this.state.next(WorkerStatus.disconnected);
        this.state.complete();
        if (this.currentWork && this.currentWork.length) {
            this.currentWork.forEach((aWork) => {
                this.mFailedWorkStream.next(aWork);
            });
        }
        this.currentWork = [];
    }

    private updateStatusForThreadChange() {
        this.stateStream.next(this.threadsWorking === this.numOfThreads ? WorkerStatus.working : WorkerStatus.workingAvailableThreads);
    }
}