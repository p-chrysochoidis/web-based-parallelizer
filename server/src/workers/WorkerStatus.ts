export enum WorkerStatus {
    notConnected,
    connected,
    ready,
    workingAvailableThreads,
    working,
    disconnected
}