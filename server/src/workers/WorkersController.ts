import { Worker } from "./Worker";
import { Server } from "../communications/Server";
import { Logger } from "../utils/log/Logger";
import { WorkerStatus } from "./WorkerStatus";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { Work } from "../work/Work";
import { map } from "rxjs/operators";
import { WorkerMessage } from "./WorkerMessage";

export class WorkersController {
    private allWorkers: Worker[] = [];
    private workersAvailableToWork: BehaviorSubject<Worker[]> = new BehaviorSubject<Worker[]>([]);
    private server: Server;
    private logger: Logger = Logger.gi();
    private mWorkersMessages: Subject<WorkerMessage> = new Subject();
    private mFailedWorkStream: Subject<Work> = new Subject();

    constructor(server: Server) {
        this.server = server;
        this.subscribeForNewConnections();
    }

    private subscribeForNewConnections() {
        this.server.connections.subscribe((connection) => {
            let worker = new Worker(connection, this.mFailedWorkStream);
            this.handleWorkerMessages(worker);
            this.handleWorkerState(worker);
        });
    }

    private handleWorkerMessages(worker: Worker): void {
        worker.onMessage.subscribe(
            (message) => {
                this.mWorkersMessages.next({
                    worker: worker,
                    message: message
                });
            },
            (error) => {
                this.logger.debug(`Worker ${worker.id} error`, error);
                worker.disconnected();
            },
            () => {
                this.logger.debug(`Worker ${worker.id} completed`);
            }
        );
    }

    private handleWorkerState(worker: Worker): void {
        worker.state.subscribe(
            (state) => {
                this.logger.debug(`Worker ${worker.id} new state: ${WorkerStatus[state]}`);
                switch (state) {
                    case WorkerStatus.connected: {
                        this.allWorkers.push(worker);
                        break;
                    }
                    case WorkerStatus.ready:
                    case WorkerStatus.workingAvailableThreads: {
                        this.workerAvailable(true, worker);
                        break;
                    }
                    case WorkerStatus.working: {
                        this.workerAvailable(false, worker);
                        break;
                    }
                    case WorkerStatus.disconnected: {
                        this.workerAvailable(false, worker);
                        let workerIndex = this.allWorkers.indexOf(worker);
                        this.logger.debug(`Worker index: ${workerIndex}`);
                        if (workerIndex > -1) {
                            this.allWorkers.splice(workerIndex, 1);
                        }
                    }
                }
                //TODO: handle state
            },
            () => {},
            () => {
                this.logger.debug(`Worker ${worker.id} state completed`);
            }
        )
    }

    private workerAvailable(available: boolean, worker: Worker) {
        let index = this.workersAvailableToWork.value.indexOf(worker);
        if (available) {
            if( index === -1) {
                this.workersAvailableToWork.value.push(worker);
            }
        }else {
            if (index > -1) {
                this.workersAvailableToWork.value.splice(index, 1);
            }
        }
        this.workersAvailableToWork.next(this.workersAvailableToWork.value);
    }

    public get failedWorkStream (): Subject<Work> {
        return this.mFailedWorkStream;
    }

    public get numOfWorkersAvailable (): Observable<number> {
        return this.workersAvailableToWork.pipe(map((workers) => workers.length));
    }

    public get numOfWorkerProcessesAvailable (): Observable<number> {
        return this.workersAvailableToWork
            .pipe(map((workers) => {
                return workers.reduce((acc, currentWorker: Worker) => acc + currentWorker.availableProcesses, 0);
            }));
    }

    public get workersAvailable (): Observable<Worker[]> {
        return this.workersAvailableToWork;
    }

    public get workersMessages (): Observable<WorkerMessage> {
        return this.mWorkersMessages;
    }
}