import { Worker } from "./Worker";
import { IncommingMessage } from "../communications/messages/incomming/IncommingMessage";

export interface WorkerMessage {
    worker: Worker;
    message: IncommingMessage;
}