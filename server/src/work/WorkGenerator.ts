import { Subject, combineLatest } from "rxjs";
import { Work } from "./Work";
import { WorkScheduler } from "./WorkScheduler";
import { Worker } from "../workers/Worker";
import { WorkGenerationContract } from "./WorkGenerationContract";
import { takeUntil, debounceTime, take, filter, delay, tap } from "rxjs/operators";
import { Logger } from "../utils/log/Logger";
import { ServerStatsController } from "../utils/ServerStatsController";
import { WorkStorage } from "./WorkStorage";
import { WorkResultHandler } from "./WorkResultHandler";

export class WorkGenerator {
    private mLogger: Logger = Logger.gi();
    private readonly GEN_THRESHOLD = 1000;
    private mWorkScheduler: WorkScheduler;
    private mWorkGenerationImpl: WorkGenerationContract;
    private mWorkStorage: WorkStorage;
    private mWorkResultHandler: WorkResultHandler;
    private mCurrentCircleWorkSupplyCompleted: Subject<boolean> = new Subject();
    private mServerStatsController: ServerStatsController = ServerStatsController.gi();
    private mIsWorkGenerationStarted: boolean = false;
    private mAllCurrentWorkCircleIdsToBeSentToWorkScheduler: string[] = [];
    private mAllWorkGenerationCompleted: Subject<boolean> = new Subject();

    constructor(workScheduler: WorkScheduler, workGenerationImpl: WorkGenerationContract, workStorage: WorkStorage, workResultHandler: WorkResultHandler) {
        this.mWorkScheduler = workScheduler;
        this.mWorkGenerationImpl = workGenerationImpl;
        this.mWorkStorage = workStorage;
        this.mWorkResultHandler = workResultHandler;
        this.mWorkGenerationImpl.attachWorkGenerator(this);
        this.signalWorkGenerationCompletionWhenDone();
        this.detectWhenWorkCirleIsCompletedAndNotifyForMoreWork();
    }

    public allWorkGenerationCompleted() {
        this.mAllWorkGenerationCompleted.next(true);
    }

    public registerWork(): void {
        if (this.mIsWorkGenerationStarted) {
            this.mLogger.info(`[WorkGenerator] Work already started.`);
            return;
        }
        this.mIsWorkGenerationStarted = true;
        this.mServerStatsController.workGenerationStarted();
        this.mWorkGenerationImpl.generateWork();
        this.mServerStatsController.workGenerationCompleted();
        this.sentWorkToSchedulerInBatches();
    }

    public submitWork(work: Work): void {
        this.mWorkStorage.addWork(work);
        this.mAllCurrentWorkCircleIdsToBeSentToWorkScheduler.push(work.id);
    }

    public workCompleted(work: Work, worker: Worker, clientProcesstime: number): void {
        this.mWorkScheduler.workCompleted(work, worker, clientProcesstime);
    }

    private detectWhenWorkCirleIsCompletedAndNotifyForMoreWork() {
        this.mWorkScheduler.areAllWorksCompletedForCurrentCirlce
            .pipe(filter((isCompleted) => isCompleted), takeUntil(this.mAllWorkGenerationCompleted.pipe(delay(0))))
            .subscribe(() => {
                this.mLogger.debug(`[WorkGenerator] Current work circle completed notifying for handling work results`);
                this.mServerStatsController.workGenerationStarted();
                let moreWorkAdded = this.mWorkGenerationImpl.handleCompletedWorkResults(this.mWorkResultHandler);
                this.mServerStatsController.workGenerationCompleted();
                if (moreWorkAdded) {
                    this.mLogger.debug(`[WorkGenerator] More work added sentWorkToSchedulerInBatches`);
                    this.sentWorkToSchedulerInBatches();
                }
            });
    }

    private sentWorkToSchedulerInBatches() {
        this.mServerStatsController.generatedWorkFeedStarted();
        this.mCurrentCircleWorkSupplyCompleted.next(false);
        this.mWorkScheduler.totalAvailableWork
            .pipe(debounceTime(0), takeUntil(this.mCurrentCircleWorkSupplyCompleted.pipe(filter((isCompleted) => isCompleted))))
            .subscribe((availableWork) => {
                this.mLogger.debug(`[WorkGenerator] Total generated work available: ${availableWork}`);
                if (availableWork < this.GEN_THRESHOLD) {
                    this.mServerStatsController.generatedWorkFeedBatchStarted();
                    this.mLogger.debug(`[WorkGenerator] Total generated work available under threshold: ${this.GEN_THRESHOLD}`);
                    let totalSent = 0;
                    while (this.mAllCurrentWorkCircleIdsToBeSentToWorkScheduler.length && totalSent++ < this.GEN_THRESHOLD) {
                        let aWorkId = this.mAllCurrentWorkCircleIdsToBeSentToWorkScheduler.shift();
                        this.mWorkScheduler.addWork(aWorkId);
                    }
                    this.mServerStatsController.generatedWorkFeedBatchCompleted();
                    if (!this.mAllCurrentWorkCircleIdsToBeSentToWorkScheduler.length) {
                        this.mCurrentCircleWorkSupplyCompleted.next(true);
                        this.mServerStatsController.generatedWorkFeedCompleted();
                        this.mLogger.debug(`[WorkGenerator] No more work to feed for this circle.`);
                    }
                }
            },
                () => { },
                () => {
                    this.mLogger.debug(`[WorkGenerator] sentWorkToSchedulerInBatches completed`);
                });
    }

    private signalWorkGenerationCompletionWhenDone() {
        combineLatest(
            this.mAllWorkGenerationCompleted.pipe(
                delay(0),
                filter((isCompleted) => isCompleted),
                take(1)
            ),
            this.mCurrentCircleWorkSupplyCompleted
        ).pipe(
            filter((data) => data[1]),
            take(1)
        ).subscribe((data) => {
            this.mLogger.debug(`All work generation completed and sent to work scheduler for all circles`, data);
            this.mServerStatsController.allWorkGenerationCompleted();
            this.mWorkScheduler.workGenerationAndSupplyCompleted();
        });
    }
}