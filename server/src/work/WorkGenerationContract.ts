import { WorkGenerator } from "./WorkGenerator";
import { WorkResultHandler } from "./WorkResultHandler";

/**
 * Creates all the know and unknown (created from the workResults) work
 * and call WorkGenerator.workGenerationCompleted when there is nothing
 * more to generate.
 */
export interface WorkGenerationContract {
    attachWorkGenerator(workGenerator: WorkGenerator): void;
    /**
     * This function should generate all the known work and
     * submit the work to the WorkGenerator using WorkGenerator.submitWork
     */
    generateWork(): void;
    /** 
     * Handle all work results in order to generate new works if needed.
     * Return if more work added
     */
    handleCompletedWorkResults(workResults: WorkResultHandler): boolean;
}