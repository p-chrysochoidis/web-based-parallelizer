import { Work } from "./Work";
import { WorkResult } from "./WorkResult";
import { Logger } from "../utils/log/Logger";
import { WorkScheduler } from "./WorkScheduler";
import { Subscription } from "rxjs";
import { take, filter, delay } from "rxjs/operators";
import { ServerStatsController } from "../utils/ServerStatsController";

export abstract class WorkResultHandler {
    protected logger: Logger = Logger.gi();
    private mWorkResults: WorkResult[] = [];
    private mWorkScheduler: WorkScheduler;
    private mWorksSchedulerSub: Subscription | null;
    private mServerStatsController: ServerStatsController = ServerStatsController.gi();

    public clearWorkResults() {
        this.mWorkResults = [];
    }

    public get workResults (): WorkResult[] {
        return this.mWorkResults;
    }

    public setWorkScheduler(workScheduler: WorkScheduler): void {
        this.mWorkScheduler = workScheduler;
        if (this.mWorksSchedulerSub) {
            this.mWorksSchedulerSub.unsubscribe();
            this.mWorksSchedulerSub = null;
        }
        this.mWorksSchedulerSub = this.mWorkScheduler.areAllWorksCompleted.pipe(
            filter((completed) => completed),
            take(1),
            delay(0)
        )
            .subscribe((completed) => {
                this.logger.info(`All works completed: ${completed}`);
                this.mServerStatsController.resultHandlingStarted();
                this.allCompletedHandleResults(this.mWorkResults);
                this.mServerStatsController.resultHandlingCompleted();
            });
    }

    public workCompleted(workResult: WorkResult) {
        this.logger.debug(`Handling result for: ${JSON.stringify(workResult)}`);
        this.mWorkResults.push(workResult);
    }

    public abstract allCompletedHandleResults(allResults: WorkResult[]): void;

    public abstract desirializeWorkResult(workResultJson: any, work: Work): WorkResult;
}