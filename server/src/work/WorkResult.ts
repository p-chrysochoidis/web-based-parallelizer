import { Work } from "./Work";

export abstract class WorkResult {
    protected mWork: Work;

    constructor(work: Work) {
        this.mWork = work;
    }

    public get work (): Work {
        return this.mWork;
    }
}