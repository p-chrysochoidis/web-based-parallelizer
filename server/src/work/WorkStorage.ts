import { Work } from "./Work";

export class WorkStorage {
    private mAllWorkMap: Map<string, Work>;

    constructor() {
        this.mAllWorkMap = new Map();
    }

    public addWork(work: Work): void {
        this.mAllWorkMap.set(work.id, work);
    }

    public getWork(workId: string): Work {
        return this.mAllWorkMap.get(workId);
    }
}