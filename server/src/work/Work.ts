export abstract class Work {
    private mId: string;

    constructor(id: string) {
        this.mId = id;
    }

    public toJsonData(): any {
        return Object.assign(
            {id: this.mId},
            this.toJson()
        );
    }

    public get id (): string {
        return this.mId;
    }

    protected abstract toJson(): any;
}