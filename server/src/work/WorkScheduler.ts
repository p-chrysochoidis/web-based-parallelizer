import { Work } from "./Work";
import { WorkersController } from "../workers/WorkersController";
import { Logger } from "../utils/log/Logger";
import { BehaviorSubject, Observable } from "rxjs";
import { combineLatest, delay, debounceTime, map } from "rxjs/operators";
import { Worker } from "../workers/Worker";
import { WorkStorage } from "./WorkStorage";
import { ServerStatsController } from "../utils/ServerStatsController";

export class WorkScheduler {
    private availableWork: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
    private workersController: WorkersController;
    private logger: Logger = Logger.gi();
    private mCurrentRunningWork: Map<string, Worker> = new Map();
    private mWorkGenerationCompleted: boolean = false;
    private mAllWorksCompleted: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    private mAllWorksForCurrentCircleCompleted: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
    private mWorkStorage: WorkStorage;
    private mServerStatsController: ServerStatsController = ServerStatsController.gi();

    constructor(workersController: WorkersController, workStorage: WorkStorage) {
        this.workersController = workersController;
        this.mWorkStorage = workStorage;
        this.workersController.workersAvailable
            .pipe(
                delay(0),
                combineLatest(
                    this.availableWork
                        .pipe(delay(0), debounceTime(200))
                    )
            )
            .subscribe((data) => this.giveWorkToAvailableWorkers(data[0], data[1]));
        this.handleFailedWork();
    }

    public addWork(workId: string): void {
        if(this.mAllWorksForCurrentCircleCompleted.getValue() || this.mAllWorksForCurrentCircleCompleted.getValue() === null) {
            this.logger.debug(`[WorkScheduler] First work for current circle added...`);
            this.mServerStatsController.worksProcessingStarted();
            this.mAllWorksForCurrentCircleCompleted.next(false);
        }
        this.logger.debug(`[WorkScheduler] Adding work #${workId}`);
        this.availableWork.value.push(workId);
        this.availableWork.next(this.availableWork.value);
    }

    public get areAllWorksCompleted (): Observable<boolean> {
        return this.mAllWorksCompleted.pipe(delay(0));
    }

    public get areAllWorksCompletedForCurrentCirlce (): Observable<boolean> {
        return this.mAllWorksForCurrentCircleCompleted.pipe(delay(0));
    }

    public get totalAvailableWork(): Observable<number> {
        return this.availableWork.pipe(map((works) => works && works.length || 0))
    }

    public workCompleted(work: Work, worker: Worker, clientProcesstime: number): void {
        this.logger.info(`[WorkScheduler] Work #${work.id} completed at worker: #${worker.id}`);
        let assignedWorker = this.mCurrentRunningWork.get(work.id);
        if(!assignedWorker) {
            this.logger.debug(`[WorkScheduler] Work not found in current working list`);
            throw new Error(`Work #${work.id} not found in current work list.`);
        }
        if(assignedWorker.id !== worker.id) {
            this.logger.debug(`[WorkerScheduler] Assigned worker missmatch #${assignedWorker.id} != #${worker.id}`);
            throw new Error(`Assigned worker missmatch #${assignedWorker.id} != #${worker.id}`);
        }
        this.mServerStatsController.processingCompletedForWork(work.id, worker.id, clientProcesstime);
        this.mCurrentRunningWork.delete(work.id);
        this.emmitIfAllWorksCompleted();
    }

    public workGenerationAndSupplyCompleted(): void {
        this.logger.debug(`[WorkScheduler] Work generation completed.`);
        this.mWorkGenerationCompleted = true;
        this.emmitIfAllWorksCompleted();
    }

    private emmitIfAllWorksCompleted(): void {
        // TODO: maybe could find a better solution for detecting if current circle completed
        let currentWorkCircleCompleted = !this.availableWork.value.length && !this.mCurrentRunningWork.size;
        let areAllCiclesCompleted = this.mWorkGenerationCompleted && currentWorkCircleCompleted;
        this.logger.debug(`Checking if all works completed: ${areAllCiclesCompleted}, current circle: ${currentWorkCircleCompleted}`);
        if (areAllCiclesCompleted) {
            this.mServerStatsController.worksProcessingCompleted();
            this.mServerStatsController.allWorkProcessingCompleted();
            this.mAllWorksCompleted.value !== areAllCiclesCompleted && this.mAllWorksCompleted.next(areAllCiclesCompleted);
        } else if (currentWorkCircleCompleted){
            this.mServerStatsController.worksProcessingCompleted();
            this.mAllWorksForCurrentCircleCompleted.value !== currentWorkCircleCompleted && this.mAllWorksForCurrentCircleCompleted.next(currentWorkCircleCompleted);
        }
    }

    private giveWorkToAvailableWorkers(workers: Worker[], workIds: string[]): void {
        this.logger.debug(`Giving work to workers. Available workers: ${workers.length}, work: ${workIds.length}`);
        let updateWorkList: boolean = false;
        if (workers.length && workIds.length) {
            let worker = workers[0];
            let workId = workIds[0];
            if (workId) {
                this.logger.log(`Sending work ${workId} to worker: ${worker.id}`);
                this.mServerStatsController.processingStartedForWork(workId, worker.id);
                //TODO: same work to many workers?
                this.mCurrentRunningWork.set(workId, worker);
                updateWorkList = true;
                worker.workOn(this.mWorkStorage.getWork(workId));
            }
        }
        if (updateWorkList) {
            this.logger.debug(`Updating available work list`);
            workIds.shift();
            this.availableWork.next(workIds);
        }
    }

    private handleFailedWork() {
        this.workersController.failedWorkStream
            .pipe(delay(0))
            .subscribe((failedWork) => {
                this.logger.trace(`Work #${failedWork.id} failed.`);
                let workerOfWork = this.mCurrentRunningWork.get(failedWork.id);
                if (!workerOfWork) {
                    this.logger.trace(`Work #${failedWork.id} not found in current work list. Skip...`);
                    return;
                }
                this.mServerStatsController.processingFailedForWork(failedWork.id, workerOfWork.id);
                this.mCurrentRunningWork.delete(failedWork.id);
                this.availableWork.value.push(failedWork.id);
                this.availableWork.next(this.availableWork.value);
            });
    }
}