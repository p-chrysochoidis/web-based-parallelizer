import express, { Application, Request, Response } from "express";
import path from "path";
import http from "http";
import socketio from "socket.io";
import { Observable } from "rxjs";
import { share } from "rxjs/operators";
import { Logger } from "../utils/log/Logger";
import { ClientConnection } from "./ClientConnection";

export class Server {
    private staticFilesLocation: string;
    private host: string;
    private port: number;
    private expressApp: Application;
    private httpServer: http.Server;
    private webSocketServer: SocketIO.Server;
    private logger: Logger = Logger.gi();
    private connectionsStream: Observable<ClientConnection>;
    private mWebWorkerCode: string;

    constructor(staticFilesLocation: string, webWorkerCode: string, host: string, port: number) {
        this.staticFilesLocation = staticFilesLocation;
        this.mWebWorkerCode = webWorkerCode;
        this.host = host;
        this.port = port;
        this.expressApp = express();
        this.httpServer = new http.Server(this.expressApp);
        this.initializeStaticFilesServer();
        this.initializeWebSocketServer();
    }

    public start() {
        this.httpServer.listen(this.port, this.host, () => {
            this.logger.info(`Serving static files from ${this.staticFilesLocation} to http://${this.host}:${this.port}/`);
        });
    }

    private initializeStaticFilesServer() {
        this.expressApp.use("/", express.static(this.staticFilesLocation))
            .use("/api/webworker.js", (req: Request, res: Response) => {
                res.send(this.mWebWorkerCode);
            })
            .use('*', (req, res) => {
                res.sendFile(path.join(this.staticFilesLocation, 'index.html'));
            });
    }

    private initializeWebSocketServer() {
        this.webSocketServer = socketio(this.httpServer, {
            path: "/connect",
            serveClient: false,
            pingInterval: 15000,
            pingTimeout: 20000
        });
        this.connectionsStream = new Observable<ClientConnection>((observer) => {
            this.webSocketServer.on("connection", (socket) => {
                this.logger.info('New connection', socket.id);
                observer.next(new ClientConnection(socket));
            });
        }).pipe(share());
    }

    public get connections (): Observable<ClientConnection> {
        return this.connectionsStream;
    }

    public stop() {
        this.httpServer.close();
        this.webSocketServer.close();
    }
}