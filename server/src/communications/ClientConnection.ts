import { Observable } from "rxjs";
import { IncommingMessage } from "./messages/incomming/IncommingMessage";
import { Logger } from "../utils/log/Logger";
import { IncommingMessageFactory } from "./messages/incomming/IncommingMessageFactory";
import { OutgoingMessage } from "./messages/outgoing/OutgoingMessage";

export class ClientConnection {
    private socket: SocketIO.Socket;
    private messageStream: Observable<IncommingMessage>;
    private logger: Logger = Logger.gi();

    constructor(socket: SocketIO.Socket) {
        this.socket = socket;
        this.initMessagesStream();
    }

    private initMessagesStream() {
        this.messageStream = new Observable<IncommingMessage>((observer) => {
            this.socket.on("message", (message: string) => {
                this.logger.debug(`${this.socket.id}: Incomming message: ${message}`);
                try {
                    observer.next(IncommingMessageFactory.create(message));
                } catch (error) {
                    this.logger.info(`${this.socket.id}: Incomming message error.`, error);
                }
            });

            this.socket.on("error", (error) => {
                this.logger.debug(`${this.socket.id}: Client connection error: ${error}`);
                observer.error(`${this.socket.id}: Client connection error: ${error}`);
                // TODO: check if it is correct
            });

            this.socket.on("disconnecting", (reason) => {
                this.logger.debug(`${this.socket.id}: Client connection disconnecting: ${reason}`);
                observer.error(`${this.socket.id}: Client connection disconnecting: ${reason}`);
                // TODO: check if it is correct
            });

            this.socket.on("disconnect", (reason) => {
                this.logger.debug(`${this.socket.id}: Client connection disconnect: ${reason}`);
                observer.error(`${this.socket.id}: Client connection disconnect: ${reason}`);
                // TODO: check if it is correct
            });

            return () => {
                this.logger.debug(`${this.socket.id}: Disposing observable. Closing connection.`);
                this.close();
            }
        });
    }

    public get messages (): Observable<IncommingMessage> {
        return this.messageStream;
    }

    public get connectionId (): string {
        return this.socket.id;
    }

    public sendMessage(outgoingMessage: OutgoingMessage): void {
        this.socket.send(outgoingMessage.serialize());
    }

    public close() {
        this.socket.disconnect(true);
    }
}