import { OutgoingMessage } from "./OutgoingMessage";
import { Work } from "../../../work/Work";

export class StartWorkMessage extends OutgoingMessage {
    private static readonly TYPE: string = "startWork";
    private work: Work;

    constructor(work: Work) {
        super();
        this.work = work;
    }

    protected toJson(){
        return {work: this.work.toJsonData()};
    }

    protected get type (): string {
        return StartWorkMessage.TYPE;
    }
}