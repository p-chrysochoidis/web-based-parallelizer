import { OutgoingMessage } from "./OutgoingMessage";

export class RegisterForServerStatsAnswerMessage extends OutgoingMessage {
    private static readonly TYPE: string = "registerForServerStatsAnswer";
    private mIsSuccess: boolean;

    constructor(success: boolean, replyForId?: string) {
        super(replyForId);
        this.mIsSuccess = success;
    }

    protected toJson(){
        return {success: this.mIsSuccess};
    }

    protected get type (): string {
        return RegisterForServerStatsAnswerMessage.TYPE;
    }
}