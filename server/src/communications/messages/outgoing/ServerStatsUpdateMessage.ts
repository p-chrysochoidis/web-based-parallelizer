import { OutgoingMessage } from "./OutgoingMessage";
import { ServerStats } from "../../../utils/ServerStats";

export class ServerStatsUpdateMessage extends OutgoingMessage {
    private static readonly TYPE: string = "serverStats";
    private mStats: ServerStats;

    constructor(stats: ServerStats) {
        super();
        this.mStats = stats;
    }

    protected toJson(){
        return {stats: this.mStats.toJsonData()};
    }

    protected get type (): string {
        return ServerStatsUpdateMessage.TYPE;
    }
}