import uuidV4 = require('uuid/v4');

export abstract class OutgoingMessage {
    private id: string;
    private replyForId: string|null;

    constructor(replyForId: string|null = null) {
        this.id = this.generateId();
        this.replyForId = replyForId;
    }

    private generateId() {
        return uuidV4();
    }

    public serialize(): string {
        return JSON.stringify(Object.assign(
            {},
            {messageId: this.id, type: this.type},
            this.replyForId ? {replyForId: this.replyForId} : {},
            this.toJson()
        ));
    }

    protected abstract toJson(): any;

    protected abstract get type (): string;
}