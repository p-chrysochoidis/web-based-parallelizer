import { OutgoingMessage } from "./OutgoingMessage";

export class StartWorkGenerationAnswerMessage extends OutgoingMessage {
    private static readonly TYPE: string = "startWorkGenerationAnswer";
    private mIsSuccess: boolean;

    constructor(success: boolean, replyForId: string) {
        super(replyForId);
        this.mIsSuccess = success;
    }

    protected toJson(){
        return {success: this.mIsSuccess};
    }

    protected get type (): string {
        return StartWorkGenerationAnswerMessage.TYPE;
    }
}