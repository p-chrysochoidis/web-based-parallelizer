import { Logger } from "../../../utils/log/Logger";
import { Worker } from "../../../workers/Worker";
import { WorkersController } from "../../../workers/WorkersController";
import { WorkGenerator } from "../../../work/WorkGenerator";
import { WorkResultHandler } from "../../../work/WorkResultHandler";
import { WorkStorage } from "../../../work/WorkStorage";
import { WebBasedParallelizer } from "../../../WebBasedParallelizer";

export abstract class IncommingMessage {
    protected mId: string;
    protected replyForId: string;
    protected logger: Logger = Logger.gi();

    constructor(jsonMessage: any) {
        this.mId = jsonMessage.id;
        this.replyForId = jsonMessage.replyForId || null;
    }

    public get id(): string {
        return this.mId;
    }

    public abstract execute(
        worker: Worker,
        workersController: WorkersController,
        workGenerator: WorkGenerator,
        workResultHandler: WorkResultHandler,
        workStorage: WorkStorage,
        webBasedParallelizer: WebBasedParallelizer
    ): void;
}