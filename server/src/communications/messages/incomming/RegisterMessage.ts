import { IncommingMessage } from "./IncommingMessage";
import { Worker } from "../../../workers/Worker";
import { WorkersController } from "../../../workers/WorkersController";
import { WorkGenerator } from "../../../work/WorkGenerator";
import { WorkResultHandler } from "../../../work/WorkResultHandler";
import { WorkStorage } from "../../../work/WorkStorage";
import { WebBasedParallelizer } from "../../../WebBasedParallelizer";

export class RegisterMessage extends IncommingMessage {
    public static TYPE: string = 'register';
    private numOfThreads: number;

    constructor(jsonMessage: any) {
        super(jsonMessage);
        this.numOfThreads = jsonMessage.numOfThreads;
    }

    public execute(worker: Worker, workersController: WorkersController, workGenerator: WorkGenerator, workResultHandler: WorkResultHandler, workStorage: WorkStorage, webBasedParallelizer: WebBasedParallelizer) {
        this.logger.debug(`[Incoming message] Executing register. Total threads: ${this.numOfThreads}`);
        worker.register(this.numOfThreads);
    }
}