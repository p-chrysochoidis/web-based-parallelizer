import { IncommingMessage } from "./IncommingMessage";
import { Worker } from "../../../workers/Worker";
import { WorkersController } from "../../../workers/WorkersController";
import { WorkGenerator } from "../../../work/WorkGenerator";
import { Work } from "../../../work/Work";
import { WorkResultHandler } from "../../../work/WorkResultHandler";
import { WorkResult } from "../../../work/WorkResult";
import { WorkStorage } from "../../../work/WorkStorage";
import { WebBasedParallelizer } from "../../../WebBasedParallelizer";

export class WorkCompletedMessage extends IncommingMessage {
    public static TYPE: string = "workCompleted";
    private workId: string;
    private workResult: any;
    private workClientProcessTime: number;

    constructor(jsonMessage: any) {
        super(jsonMessage);
        this.workId = jsonMessage.workId;
        this.workResult = jsonMessage.workResult;
        this.workClientProcessTime = jsonMessage.processTime;
    }

    public execute(worker: Worker, workersController: WorkersController, workGenerator: WorkGenerator, workResultHandler: WorkResultHandler, workStorage: WorkStorage, webBasedParallelizer: WebBasedParallelizer) {
        this.logger.debug(`[Incoming message] Executing workCompleted. Work: ${JSON.stringify(this.workId)}, workResult: ${JSON.stringify(this.workResult)}, processTime: ${this.workClientProcessTime}`);
        let work: Work = workStorage.getWork(this.workId);
        let workResult: WorkResult = workResultHandler.desirializeWorkResult(this.workResult, work);
        worker.workCompleted(workResult);
        try {
            workGenerator.workCompleted(work, worker, this.workClientProcessTime);
            workResultHandler.workCompleted(workResult);
        } catch (e) {
            this.logger.debug(e);
        }
    }
}