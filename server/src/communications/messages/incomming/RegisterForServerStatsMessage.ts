import { IncommingMessage } from "./IncommingMessage";
import { Worker } from "../../../workers/Worker";
import { WorkersController } from "../../../workers/WorkersController";
import { WorkGenerator } from "../../../work/WorkGenerator";
import { WorkResultHandler } from "../../../work/WorkResultHandler";
import { WorkStorage } from "../../../work/WorkStorage";
import { WebBasedParallelizer } from "../../../WebBasedParallelizer";

export class RegisterForServerStatsMessage extends IncommingMessage {
    public static TYPE: string = 'registerForServerStats';

    constructor(jsonMessage: any) {
        super(jsonMessage);
    }

    public execute(worker: Worker, workersController: WorkersController, workGenerator: WorkGenerator, workResultHandler: WorkResultHandler, workStorage: WorkStorage, webBasedParallelizer: WebBasedParallelizer) {
        this.logger.debug(`[Incoming message] Executing register for server stats.`);
        worker.registerForServerStats(this);
    }
}