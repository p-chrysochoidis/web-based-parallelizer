import { IncommingMessage } from "./IncommingMessage";
import { Worker } from "../../../workers/Worker";
import { WorkersController } from "../../../workers/WorkersController";
import { WorkGenerator } from "../../../work/WorkGenerator";
import { WorkResultHandler } from "../../../work/WorkResultHandler";
import { WorkStorage } from "../../../work/WorkStorage";
import { WebBasedParallelizer } from "../../../WebBasedParallelizer";
import { StartWorkGenerationAnswerMessage } from "../outgoing/StartWorkGenerationAnswerMessage";

export class StartWorkGenerationMessage extends IncommingMessage {
    public static TYPE: string = 'startWorkGeneration';

    constructor(jsonMessage: any) {
        super(jsonMessage);
    }

    public execute(worker: Worker, workersController: WorkersController, workGenerator: WorkGenerator, workResultHandler: WorkResultHandler, workStorage: WorkStorage, webBasedParallelizer: WebBasedParallelizer) {
        this.logger.debug(`[Incoming message] Executing startWorkGeneration message.`);
        webBasedParallelizer.startWork();
        worker.sendMessage(new StartWorkGenerationAnswerMessage(true, this.id));
    }
}