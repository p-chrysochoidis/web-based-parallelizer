import { IncommingMessage } from "./IncommingMessage";
import { RegisterMessage } from "./RegisterMessage";
import { WorkCompletedMessage } from "./WorkCompletedMessage";
import { RegisterForServerStatsMessage } from "./RegisterForServerStatsMessage";
import { StartWorkGenerationMessage } from "./StartWorkGenerationMessage";

export class IncommingMessageFactory {
    public static create(message: string): IncommingMessage {
        let jsonMessage = JSON.parse(message),
            messageType = jsonMessage && jsonMessage.type || null;

            switch(messageType){
                case RegisterMessage.TYPE:
                    return new RegisterMessage(jsonMessage);
                case WorkCompletedMessage.TYPE:
                    return new WorkCompletedMessage(jsonMessage);
                case RegisterForServerStatsMessage.TYPE:
                    return new RegisterForServerStatsMessage(jsonMessage);
                case StartWorkGenerationMessage.TYPE:
                    return new StartWorkGenerationMessage(jsonMessage);
            }

            throw new Error(`Incomming message type not found: ${messageType}`);
    }
}