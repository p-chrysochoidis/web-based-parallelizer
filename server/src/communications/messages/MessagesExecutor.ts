import { WorkersController } from "../../workers/WorkersController";
import { WorkGenerator } from "../../work/WorkGenerator";
import { WorkScheduler } from "../../work/WorkScheduler";
import { WorkResultHandler } from "../../work/WorkResultHandler";
import { Subscription } from "rxjs";
import { WorkStorage } from "../../work/WorkStorage";
import { WebBasedParallelizer } from "../../WebBasedParallelizer";

export class MessagesExecutor {
    private mWorkersController: WorkersController;
    private mWorkGenerator: WorkGenerator;
    private mWorkScheduler: WorkScheduler;
    private mWorkResultHandler: WorkResultHandler;
    private mWorkStorage: WorkStorage;
    private mWebBasedParallelizer: WebBasedParallelizer;
    private mIncomingMessagesSubscription: Subscription | null;

    constructor(
        workersController: WorkersController,
        workGenerator: WorkGenerator,
        workScheduler: WorkScheduler,
        workResultHandler: WorkResultHandler,
        workStorage: WorkStorage,
        webBasedParallelizer: WebBasedParallelizer
    ) {
        this.mWorkersController = workersController;
        this.mWorkGenerator = workGenerator;
        this.mWorkScheduler = workScheduler;
        this.mWorkResultHandler = workResultHandler;
        this.mWorkStorage = workStorage;
        this.mWebBasedParallelizer = webBasedParallelizer;
        this.executeIncomingMessages();
    }

    public shutdown(): void {
        this.mIncomingMessagesSubscription && 
        !this.mIncomingMessagesSubscription.closed &&
        this.mIncomingMessagesSubscription.unsubscribe();
        this.mIncomingMessagesSubscription = null;
    }

    private executeIncomingMessages(): void {
        this.mIncomingMessagesSubscription = this.mWorkersController.workersMessages.subscribe((workerMessage) => {
            workerMessage.message.execute(
                workerMessage.worker,
                this.mWorkersController,
                this.mWorkGenerator,
                this.mWorkResultHandler,
                this.mWorkStorage,
                this.mWebBasedParallelizer
            );
        });
    }
}