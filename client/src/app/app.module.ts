import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppSharedModule } from './shared/app.shared.module';
import { AppRoutingModule } from './app-routing.module';
import { WorkerUiModule } from './worker-ui/worker-ui.module';
import { ConnectionStatusComponent } from './connection-status.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ServerUiModule } from './server-ui/server-ui.module';

@NgModule({
  declarations: [
    AppComponent,
    ConnectionStatusComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    AppSharedModule,
    WorkerUiModule,
    ServerUiModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
