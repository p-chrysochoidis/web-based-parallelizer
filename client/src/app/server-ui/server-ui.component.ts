import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServerUiService } from './server-ui.service';
import { Subscription, Observable } from 'rxjs';
import { ServerStats } from './models/server-stats.model';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { ServerStatsViewData } from './models/server-stats-view-data.model';

@Component({
    templateUrl: './server-ui.component.html',
    styleUrls: ['./server-ui.component.scss']
})
export class ServerUiComponent implements OnInit, OnDestroy {
    private mStatsSubscription: Subscription;
    private mStartWorkGenerationSubscription: Subscription;
    public isLoading: boolean = true;
    public hasError: boolean = false;
    public stats: ServerStats;
    public faSpinner = faSpinner;
    public isStartWorkGenerationInProgress: boolean = false;
    public isStartButtonDisabledUntilNextStats: boolean = false;
    public statsViewData: ServerStatsViewData;
    private mStatsStream: Observable<ServerStats>;

    constructor(private mServerUiService: ServerUiService) {
    }

    public startWorkGeneration() {
        if (this.isStartWorkGenerationInProgress) {
            return;
        }
        this.isStartWorkGenerationInProgress = true;
        this.mStartWorkGenerationSubscription = this.mServerUiService.startWorkGeneration()
            .subscribe(
                () => {},
                (error) => {
                    console.log(error);
                    this.isStartWorkGenerationInProgress = false;
                },
                () => {
                    this.isStartButtonDisabledUntilNextStats = true;
                    this.isStartWorkGenerationInProgress = false;
                }
            );
    }

    ngOnInit() {
        this.mStatsStream = this.mServerUiService.getServerStats();
         this.statsViewData = new ServerStatsViewData(this.mServerUiService, this.mStatsStream);

        this.mStatsSubscription = this.mStatsStream.subscribe(
            (stats) => {
                this.isLoading = false;
                this.isStartButtonDisabledUntilNextStats = false;
                this.stats = stats;
            },
            (error) => {
                console.log(error);
                this.isLoading = false;
                this.hasError = true;
            }
        );
    }

    ngOnDestroy() {
        if (this.mStatsSubscription && !this.mStatsSubscription.closed) {
            this.mStatsSubscription.unsubscribe();
        }
        if (this.mStartWorkGenerationSubscription && !this.mStartWorkGenerationSubscription.closed) {
            this.mStartWorkGenerationSubscription.unsubscribe();
        }
        this.mStatsSubscription = null;
        this.mStartWorkGenerationSubscription = null;
    }
}
