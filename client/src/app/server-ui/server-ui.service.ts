import { Injectable } from '@angular/core';
import { SocketService } from '../shared/socket.service';
import { ServerStats } from './models/server-stats.model';
import { Observable } from 'rxjs';
import { filter, map, switchMap, refCount, publishReplay } from 'rxjs/operators';
import { SocketMessageTypes } from '../shared/models/socket-message-types.enum';
import { Duration } from './models/duration.model';
import { TimedValue } from './models/timed-value.model';
import * as prettyMs from 'pretty-ms';
import { WorkersAndWorkTotalsFormated } from './models/workers-and-work-totals-formated.model';
import { WorkersTotals } from './models/workers-totals.model';
import { WorkTotals } from './models/work-totals.model';
import { DurationTotalWithStatsFormated } from './models/duration-total-with-stats-formated.model';
import { DurationTotalWithStats } from './models/duration-total-with-stats.model';

@Injectable()
export class ServerUiService {

    constructor(private mSocketService: SocketService) {
    }

    public getServerStats(): Observable<ServerStats> {
        return this.mSocketService.registerForServerStats()
            .pipe(
                switchMap(() => this.mSocketService.messages),
                filter((message) => message.type === SocketMessageTypes.serverStats),
                map((message) => {
                    return this.getServerStatsFromMessage(message);
                }),
                publishReplay(1),
                refCount()
            );
    }

    public startWorkGeneration(): Observable<void> {
        return this.mSocketService.sendStartWorkGenerationMessage();
    }

    private getServerStatsFromMessage(message: any): ServerStats {
        const statsData = message.stats;
        const stats: ServerStats = statsData;
        stats.workGenerationDuration = this.getDuration(stats.workGenerationDuration);
        stats.totalDuration = this.getDuration(stats.totalDuration);
        stats.workProcessingDuration = this.getDuration(stats.workProcessingDuration);
        stats.resultHandlingDuration = this.getDuration(stats.resultHandlingDuration);
        stats.workGenerationsCirclesDurations = stats.workGenerationsCirclesDurations.map(
            (aCircleDuration) => this.getDuration(aCircleDuration)
        );
        stats.generatedWorkFeedCirclesDurations = stats.generatedWorkFeedCirclesDurations.map(
            (wholeAndBatches) => {
                return {
                    wholeCircle: this.getDuration(wholeAndBatches.wholeCircle),
                    batches: wholeAndBatches.batches.map((batch) => this.getDuration(batch))
                };
            }
        );
        stats.workCircleProcessingDurations = stats.workCircleProcessingDurations.map(
            (aCircleDuration) => this.getDuration(aCircleDuration)
        );
        stats.individualWorkProcessingDurations = stats.individualWorkProcessingDurations.map(
            (aWorkIdWithDuration) => {
                return {
                    workId: aWorkIdWithDuration.workId,
                    duration: this.getDuration(aWorkIdWithDuration.duration)
                };
            }
        );
        stats.individualWorkProcessingTotalsPerWorkerInTime = stats.individualWorkProcessingTotalsPerWorkerInTime.map(
            (workerInTime) => {
                return {
                    workerId: workerInTime.workerId,
                    totals: {
                        processing: workerInTime.totals.processing.map((tv) => this.getTimedValue(tv)),
                        completed: workerInTime.totals.completed.map((tv) => this.getTimedValue(tv)),
                        failed: workerInTime.totals.failed.map((tv) => this.getTimedValue(tv)),
                    }
                };
            }
        );
        stats.latencyByWorkerInTime = stats.latencyByWorkerInTime.map(
            (workerInTime) => {
                return {
                    workerId: workerInTime.workerId,
                    totals: {
                        total: workerInTime.totals.total.map((tv) => this.getTimedValue(tv)),
                        average: workerInTime.totals.average.map((tv) => this.getTimedValue(tv)),
                        min: workerInTime.totals.min.map((tv) => this.getTimedValue(tv)),
                        max: workerInTime.totals.max.map((tv) => this.getTimedValue(tv)),
                    }
                };
            }
        );
        return stats;
    }

    public getWorkGenerationStarted(statsStream: Observable<ServerStats>): Observable<boolean> {
        return statsStream.pipe(map((stats) => stats.workGenerationStarted));
    }

    public getWorkGenerationCompleted(statsStream: Observable<ServerStats>): Observable<boolean> {
        return statsStream.pipe(map((stats) => stats.workGenerationCompleted));
    }

    public getWorkProcessingStarted(statsStream: Observable<ServerStats>): Observable<boolean> {
        return statsStream.pipe(map((stats) => stats.workProcessingStarted));
    }

    public getWorkProcessingCompleted(statsStream: Observable<ServerStats>): Observable<boolean> {
        return statsStream.pipe(map((stats) => stats.workProcessingCompleted));
    }

    public getWorkAllCompleted(statsStream: Observable<ServerStats>): Observable<boolean> {
        return statsStream.pipe(map((stats) => stats.allDone));
    }

    public getWorkGenerationDuration(statsStream: Observable<ServerStats>): Observable<string> {
        return statsStream.pipe(map((stats) => stats.workGenerationDuration
            && stats.workGenerationDuration.duration !== null
            && prettyMs(stats.workGenerationDuration.duration) || '...'));
    }

    public getWorkProcessingDuration(statsStream: Observable<ServerStats>): Observable<string> {
        return statsStream.pipe(map((stats) => stats.workProcessingDuration
            && stats.workProcessingDuration.duration !== null
            && prettyMs(stats.workProcessingDuration.duration) || '...'));
    }

    public getWorksResultHandlingDuration(statsStream: Observable<ServerStats>): Observable<string> {
        return statsStream.pipe(map((stats) => stats.resultHandlingDuration
            && stats.resultHandlingDuration.duration !== null
            && prettyMs(stats.resultHandlingDuration.duration) || '...'));
    }

    public getWorkTotalDuration(statsStream: Observable<ServerStats>): Observable<string> {
        return statsStream.pipe(map((stats) => stats.totalDuration
            && stats.totalDuration.duration !== null
            && prettyMs(stats.totalDuration.duration) || '...'));
    }

    public getWorkAndWorkersTotals(statsStream: Observable<ServerStats>): Observable<WorkersAndWorkTotalsFormated> {
        return statsStream.pipe(map((stats) => {
            const workers: WorkersTotals = stats.workAndWorkersTotals.workers;
            const work: WorkTotals = stats.workAndWorkersTotals.work;
            return {
                workers: {
                    totalConnected: workers.totalConnected,
                    totalProcesses: workers.totalProcesses,
                    available: workers.available,
                    availableProcesses: workers.availableProcesses
                },
                work: {
                    totalCompleted: work.totalCompleted,
                    totalInProgress: work.totalInProgress,
                    totalFailed: work.totalFailed,
                    totalWaitingProcessing: work.totalWaitingProcessing,
                    duration: this.getDurationTotalWithStatsFormated(work.duration),
                    clientDuration: this.getDurationTotalWithStatsFormated(work.clientDuration),
                    latency: this.getDurationTotalWithStatsFormated(work.latency),
                }
            };
        }));
    }

    private getDuration(durationData: Duration): Duration {
        if (!durationData) {
            return null;
        }
        return {
            startedAt: durationData.startedAt && new Date(durationData.startedAt),
            endedAt: durationData.endedAt && new Date(durationData.endedAt),
            duration: durationData.duration === -1 ? null : durationData.duration,
            otherSourceDuration: durationData.otherSourceDuration,
            durationsDiff: durationData.durationsDiff,
        };
    }

    private getTimedValue<T>(timedValue: TimedValue<T>): TimedValue<T> {
        return {
            date: new Date(timedValue.date),
            value: timedValue.value
        };
    }

    private getDurationTotalWithStatsFormated(d: DurationTotalWithStats): DurationTotalWithStatsFormated {
        return {
            total: prettyMs(d.total),
            average: prettyMs(d.average),
            min: d.min !== null && prettyMs(d.min) || '',
            max: prettyMs(d.max),
        };
    }
}
