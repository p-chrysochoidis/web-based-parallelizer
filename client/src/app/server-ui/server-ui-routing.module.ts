import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServerUiComponent } from './server-ui.component';

const workerUiRoutes: Routes = [
    { path: 'server', component: ServerUiComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(workerUiRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ServerUiRoutingModule { }
