import { NgModule } from '@angular/core';
import { ServerUiRoutingModule } from './server-ui-routing.module';
import { ServerUiComponent } from './server-ui.component';
import { ServerUiService } from './server-ui.service';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppSharedModule } from '../shared/app.shared.module';

@NgModule({
    declarations: [
        ServerUiComponent
    ],
    providers: [
        ServerUiService
    ],
    imports: [
        ServerUiRoutingModule,
        CommonModule,
        FontAwesomeModule,
        AppSharedModule
    ]
})
export class ServerUiModule {}
