export interface DurationTotalWithStats {
    total: number;
    average: number;
    min: number;
    max: number;
}
