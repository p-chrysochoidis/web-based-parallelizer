import { WorkTotals } from './work-totals.model';
import { WorkersTotals } from './workers-totals.model';

export interface WorkersAndWorkTotals {
    workers: WorkersTotals;
    work: WorkTotals;
}
