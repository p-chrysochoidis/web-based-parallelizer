import { DurationTotalWithStatsFormated } from './duration-total-with-stats-formated.model';

export interface WorkTotalsFormated {
    totalCompleted: number;
    totalInProgress: number;
    totalFailed: number;
    totalWaitingProcessing: number;
    duration: DurationTotalWithStatsFormated;
    clientDuration: DurationTotalWithStatsFormated;
    latency: DurationTotalWithStatsFormated;
}
