export interface TimedValue<T> {
    date: Date;
    value: T;
}
