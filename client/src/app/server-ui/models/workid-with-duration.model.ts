import { Duration } from './duration.model';

export interface WorkIdWithDuration {
    workId: string;
    duration: Duration;
}
