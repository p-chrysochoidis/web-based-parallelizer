import { DurationTotalWithStats } from './duration-total-with-stats.model';

export interface WorkerIdTotalsWithStats {
    workerId: string;
    totals: DurationTotalWithStats;
}
