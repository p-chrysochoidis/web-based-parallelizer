import { Observable } from 'rxjs';
import { ServerStats } from '../server-stats.model';
import { ChartData, ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { ChartDataModel } from '../../../shared/models/chart-data.model';
import { map } from 'rxjs/operators';
import { WorkIdWithDuration } from '../workid-with-duration.model';

export class IndividualWorkProcessingChart implements ChartDataModel {
    public chartData: Observable<ChartData>;
    public chartOptions: ChartOptions = this.createChartOptions();
    public chartType: ChartType = <any> 'scatter';
    private mLabels: string[] = [];
    private readonly mDataset: ChartDataSets[] = this.initDatasets();

    constructor(stats: Observable<ServerStats>) {
        this.chartData = this.initChartData(stats);
    }

    private initDatasets(): ChartDataSets[] {
        return [
            {data: [], backgroundColor: '#4453c460', borderColor: '#4453c4', label: 'Master computed time'},
            {data: [], backgroundColor: '#80808060', borderColor: 'gray', label: 'Worker computed time'}
        ];
    }

    private initChartData(statsStream: Observable<ServerStats>): Observable<ChartData> {
        return statsStream.pipe(
            map((stats) => {
                return stats.individualWorkProcessingDurations;
            }),
            map((workIdsWithDurations: WorkIdWithDuration[]) => {
                const masterTimesData: any = this.mDataset[0].data;
                const workerTimesData: any = this.mDataset[1].data;
                if (workIdsWithDurations.length) {
                   workIdsWithDurations.forEach((idWithDuration, index) => {
                        this.mLabels[index] = `#${idWithDuration.workId}`;
                        masterTimesData[index] = idWithDuration.duration && idWithDuration.duration.duration || 0;
                        workerTimesData[index] = idWithDuration.duration && idWithDuration.duration.otherSourceDuration || 0;
                    });
                } else {
                    this.mLabels = [];
                    this.mDataset[0].data = [];
                    this.mDataset[1].data = [];
                }
                return {
                    labels: this.mLabels,
                    datasets: this.mDataset,
                };
            })
        );
    }

    private createChartOptions() {
        return {
            responsive: false,
            title: { display: true, text: 'Work process duration (ms)' },
            legend: { display: true },
            scales: {
                xAxes: [
                    {
                        type: 'category',
                        ticks: {
                            beginAtZero: true
                        }
                    }
                ],
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true
                        }
                    }
                ]
            },
        };
    }
}
