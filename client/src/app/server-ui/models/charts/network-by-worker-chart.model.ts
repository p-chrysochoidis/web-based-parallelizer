import { Observable } from 'rxjs';
import { ServerStats } from '../server-stats.model';
import { ChartData, ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { ChartDataModel } from '../../../shared/models/chart-data.model';
import { map } from 'rxjs/operators';
import { WorkerIdTotalsWithStats } from '../worker-id-totals-with-stats.model';

export class NetworkByWorkerChart implements ChartDataModel {
    public chartData: Observable<ChartData>;
    public chartOptions: ChartOptions = this.createChartOptions();
    public chartType: ChartType = 'bar';
    private mLabels: string[] = [];
    private readonly mDataset: ChartDataSets[] = this.initDatasets();

    constructor(stats: Observable<ServerStats>) {
        this.chartData = this.initChartData(stats);
    }

    private initDatasets(): ChartDataSets[] {
        return [
            {data: [], backgroundColor: 'gray', borderColor: 'gray', label: 'Total', yAxisID: '0'},
            {data: [], backgroundColor: 'orange', borderColor: 'orange', label: 'Average', yAxisID: '1'},
            {data: [], backgroundColor: 'green', borderColor: 'green', label: 'min', yAxisID: '1'},
            {data: [], backgroundColor: 'yellow', borderColor: 'yellow', label: 'max', yAxisID: '1'}
        ];
    }

    private initChartData(statsStream: Observable<ServerStats>): Observable<ChartData> {
        return statsStream.pipe(
            map((stats) => {
                return stats.latencyByWorker;
            }),
            map((workerIdsTotalsWithStats: WorkerIdTotalsWithStats[]) => {
                const totalData: any = this.mDataset[0].data;
                const averageData: any = this.mDataset[1].data;
                const minData: any = this.mDataset[2].data;
                const maxData: any = this.mDataset[3].data;
                if (workerIdsTotalsWithStats.length) {
                   workerIdsTotalsWithStats.forEach((idWithTotals, index) => {
                        this.mLabels[index] = `#${idWithTotals.workerId}`;
                        totalData[index] = idWithTotals.totals && idWithTotals.totals.total || 0;
                        averageData[index] = idWithTotals.totals && idWithTotals.totals.average || 0;
                        minData[index] = idWithTotals.totals && idWithTotals.totals.min || 0;
                        maxData[index] = idWithTotals.totals && idWithTotals.totals.max || 0;
                    });
                } else {
                    this.mLabels = [];
                    this.mDataset[0].data = [];
                    this.mDataset[1].data = [];
                    this.mDataset[2].data = [];
                    this.mDataset[3].data = [];
                }
                return {
                    labels: this.mLabels,
                    datasets: this.mDataset,
                };
            })
        );
    }

    private createChartOptions() {
        return {
            responsive: false,
            title: { display: true, text: 'Network totals per worker (ms)' },
            legend: { display: true },
            scales: {
                yAxes: [
                    {
                        id: '0',
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: { display: false },
                        position: 'left'
                    },
                    {
                        id: '1',
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: { display: false },
                        position: 'right'
                    }
                ]
            }
        };
    }
}
