import { Observable } from 'rxjs';
import { ServerStats } from '../server-stats.model';
import { ChartData, ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { ChartDataModel } from '../../../shared/models/chart-data.model';
import { map } from 'rxjs/operators';
import { Duration } from '../duration.model';
import randomColor from 'randomcolor';

export class WorkFeedDurationPerCircle implements ChartDataModel {
    public chartData: Observable<ChartData>;
    public chartOptions: ChartOptions = this.createChartOptions();
    public chartType: ChartType = 'pie';
    private mLabels: string[] = [];
    private readonly mDataset: ChartDataSets[] = this.initDatasets();

    constructor(stats: Observable<ServerStats>) {
        this.chartData = this.initChartData(stats);
    }

    private initDatasets(): ChartDataSets[] {
        return [
            {data: [], backgroundColor: [], borderColor: 'gray'}
        ];
    }

    private initChartData(statsStream: Observable<ServerStats>): Observable<ChartData> {
        return statsStream.pipe(
            map((stats) => {
                return stats.generatedWorkFeedCirclesDurations
                    .map((circleWithBatch) => {
                        return circleWithBatch.wholeCircle;
                    });
            }),
            map((workPerCircle: Duration[]) => {
                const dataset: ChartDataSets = this.mDataset[0];
                const data: any = dataset.data;
                const bgColor: any = dataset.backgroundColor;
                const startIndex = this.mLabels.length; // this.mLabels data and bgColor must have always the same length
                let index = startIndex;
                let forceStop = false;
                let workCircle: Duration;
                const totalCircles = workPerCircle.length;
                if (!totalCircles) {
                    this.mLabels = [];
                    dataset.data = [];
                    dataset.backgroundColor = [];
                }
                while (index < totalCircles) {
                    workCircle = workPerCircle[index];
                    forceStop = !workCircle || workCircle.duration === null;
                    if (!forceStop) {
                        this.mLabels.push(`Circle #${index}`);
                        data.push(workCircle.duration);
                        bgColor.push(randomColor({seed: index}));
                    }
                    index++;
                }
                return {
                    labels: this.mLabels,
                    datasets: this.mDataset,
                };
            })
        );
    }

    private createChartOptions() {
        return {
            responsive: false,
            title: { display: true, text: 'Genarated work feed duration per circle (ms)' }
        };
    }
}
