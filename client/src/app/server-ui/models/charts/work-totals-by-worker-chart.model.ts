import { Observable } from 'rxjs';
import { ServerStats } from '../server-stats.model';
import { ChartData, ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { ChartDataModel } from '../../../shared/models/chart-data.model';
import { map } from 'rxjs/operators';
import { WorkerIdWithTotals } from '../worker-id-with-totals.model';

export class WorkTotalsByWorkerChart implements ChartDataModel {
    public chartData: Observable<ChartData>;
    public chartOptions: ChartOptions = this.createChartOptions();
    public chartType: ChartType = 'bar';
    private mLabels: string[] = [];
    private readonly mDataset: ChartDataSets[] = this.initDatasets();

    constructor(stats: Observable<ServerStats>) {
        this.chartData = this.initChartData(stats);
    }

    private initDatasets(): ChartDataSets[] {
        return [
            {data: [], backgroundColor: 'gray', borderColor: 'gray', label: 'In process'},
            {data: [], backgroundColor: 'yellow', borderColor: 'yellow', label: 'Completed'},
            {data: [], backgroundColor: 'red', borderColor: 'red', label: 'Failed'}
        ];
    }

    private initChartData(statsStream: Observable<ServerStats>): Observable<ChartData> {
        return statsStream.pipe(
            map((stats) => {
                return stats.individualWorkProcessingTotalsPerWorker;
            }),
            map((workerIdsWithDurations: WorkerIdWithTotals[]) => {
                const inProcessData: any = this.mDataset[0].data;
                const completedData: any = this.mDataset[1].data;
                const failedData: any = this.mDataset[2].data;
                if (workerIdsWithDurations.length) {
                   workerIdsWithDurations.forEach((idWithTotals, index) => {
                        this.mLabels[index] = `#${idWithTotals.workerId}`;
                        inProcessData[index] = idWithTotals.totals && idWithTotals.totals.processing || 0;
                        completedData[index] = idWithTotals.totals && idWithTotals.totals.completed || 0;
                        failedData[index] = idWithTotals.totals && idWithTotals.totals.failed || 0;
                    });
                } else {
                    this.mLabels = [];
                    this.mDataset[0].data = [];
                    this.mDataset[1].data = [];
                    this.mDataset[2].data = [];
                }
                return {
                    labels: this.mLabels,
                    datasets: this.mDataset,
                };
            })
        );
    }

    private createChartOptions() {
        return {
            responsive: false,
            title: { display: true, text: 'Work totals by worker' },
            legend: { display: true },
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: { display: false }
                    }
                ]
            }
        };
    }
}
