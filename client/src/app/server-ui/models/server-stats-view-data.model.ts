import { ServerUiService } from '../server-ui.service';
import { ServerStats } from './server-stats.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ChartDataModel } from '../../shared/models/chart-data.model';
import { WorkGenerationPerCircle } from './charts/work-generation-per-circle-chart.model';
import { WorkFeedDurationPerCircle } from './charts/work-feed-duration-per-circle-chart.model';
import { WorkProcessingPerCircle } from './charts/work-processing-per-circle-chart.model';
import { IndividualWorkProcessingChart } from './charts/individual-work-processing-chart.model';
import { WorkTotalsByWorkerChart } from './charts/work-totals-by-worker-chart.model';
import { NetworkByWorkerChart } from './charts/network-by-worker-chart.model';

export class ServerStatsViewData {
    public workGenerationStarted: Observable<boolean>;
    public workGenerationCompleted: Observable<boolean>;
    public workGenerationDuration: Observable<string>;
    public workProcessingStarted: Observable<boolean>;
    public workProcessingCompleted: Observable<boolean>;
    public workProcessingDuration: Observable<string>;
    public worksResultHandlingDuration: Observable<string>;
    public workAllCompleted: Observable<boolean>;
    public workTotalDuration: Observable<string>;
    public totalWorkers: Observable<number>;
    public totalProcesses: Observable<number>;
    public availableWorkers: Observable<number>;
    public availableProcesses: Observable<number>;
    public totalWorkCompleted: Observable<number>;
    public totalWorkInProgress: Observable<number>;
    public totalWorkFailed: Observable<number>;
    public totalWorkWaitingProcessing: Observable<number>;
    public serverDurationTotal: Observable<string>;
    public serverDurationAverage: Observable<string>;
    public serverDurationMin: Observable<string>;
    public serverDurationMax: Observable<string>;
    public clientDurationTotal: Observable<string>;
    public clientDurationAverage: Observable<string>;
    public clientDurationMin: Observable<string>;
    public clientDurationMax: Observable<string>;
    public networkDurationTotal: Observable<string>;
    public networkDurationAverage: Observable<string>;
    public networkDurationMin: Observable<string>;
    public networkDurationMax: Observable<string>;
    public chartWorkGenerationDurationPerWorkCircle: ChartDataModel;
    public chartGeneratedWorkFeedDurationPerWorkCircle: ChartDataModel;
    public chartWorkProcessingDurationPerWorkCircle: ChartDataModel;
    public chartIndividualWorkProcessingDurations: ChartDataModel;
    public chartWorkTotalsByWorker: ChartDataModel;
    public chartNetworkTotalsByWorker: ChartDataModel;

    constructor(
        private mServerUiService: ServerUiService,
        private mStatsStream: Observable<ServerStats>
    ) {
        this.init();
    }

    private init() {
        this.workGenerationStarted = this.mServerUiService.getWorkGenerationStarted(this.mStatsStream);
        this.workGenerationCompleted = this.mServerUiService.getWorkGenerationCompleted(this.mStatsStream);
        this.workGenerationDuration = this.mServerUiService.getWorkGenerationDuration(this.mStatsStream);
        this.workProcessingStarted = this.mServerUiService.getWorkProcessingStarted(this.mStatsStream);
        this.workProcessingCompleted = this.mServerUiService.getWorkProcessingCompleted(this.mStatsStream);
        this.workProcessingDuration = this.mServerUiService.getWorkProcessingDuration(this.mStatsStream);
        this.worksResultHandlingDuration = this.mServerUiService.getWorksResultHandlingDuration(this.mStatsStream);
        this.workAllCompleted = this.mServerUiService.getWorkAllCompleted(this.mStatsStream);
        this.workTotalDuration = this.mServerUiService.getWorkTotalDuration(this.mStatsStream);

        const wNwTotals = this.mServerUiService.getWorkAndWorkersTotals(this.mStatsStream);
        this.totalWorkers = wNwTotals.pipe(map(d => d.workers.totalConnected));
        this.totalProcesses = wNwTotals.pipe(map(d => d.workers.totalProcesses));
        this.availableWorkers = wNwTotals.pipe(map(d => d.workers.available));
        this.availableProcesses = wNwTotals.pipe(map(d => d.workers.availableProcesses));
        this.totalWorkCompleted = wNwTotals.pipe(map(d => d.work.totalCompleted));
        this.totalWorkInProgress = wNwTotals.pipe(map(d => d.work.totalInProgress));
        this.totalWorkFailed = wNwTotals.pipe(map(d => d.work.totalFailed));
        this.totalWorkWaitingProcessing = wNwTotals.pipe(map(d => d.work.totalWaitingProcessing));
        this.serverDurationTotal = wNwTotals.pipe(map(d => d.work.duration.total));
        this.serverDurationAverage = wNwTotals.pipe(map(d => d.work.duration.average));
        this.serverDurationMin = wNwTotals.pipe(map(d => d.work.duration.min));
        this.serverDurationMax = wNwTotals.pipe(map(d => d.work.duration.max));
        this.clientDurationTotal = wNwTotals.pipe(map(d => d.work.clientDuration.total));
        this.clientDurationAverage = wNwTotals.pipe(map(d => d.work.clientDuration.average));
        this.clientDurationMin = wNwTotals.pipe(map(d => d.work.clientDuration.min));
        this.clientDurationMax = wNwTotals.pipe(map(d => d.work.clientDuration.max));
        this.networkDurationTotal = wNwTotals.pipe(map(d => d.work.latency.total));
        this.networkDurationAverage = wNwTotals.pipe(map(d => d.work.latency.average));
        this.networkDurationMin = wNwTotals.pipe(map(d => d.work.latency.min));
        this.networkDurationMax = wNwTotals.pipe(map(d => d.work.latency.max));

        this.chartWorkGenerationDurationPerWorkCircle = new WorkGenerationPerCircle(this.mStatsStream);
        this.chartGeneratedWorkFeedDurationPerWorkCircle = new WorkFeedDurationPerCircle(this.mStatsStream);
        this.chartWorkProcessingDurationPerWorkCircle = new WorkProcessingPerCircle(this.mStatsStream);
        this.chartIndividualWorkProcessingDurations = new IndividualWorkProcessingChart(this.mStatsStream);
        this.chartWorkTotalsByWorker = new WorkTotalsByWorkerChart(this.mStatsStream);
        this.chartNetworkTotalsByWorker = new NetworkByWorkerChart(this.mStatsStream);
    }
}
