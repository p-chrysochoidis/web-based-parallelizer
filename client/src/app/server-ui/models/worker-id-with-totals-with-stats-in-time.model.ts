import { TimedValue } from './timed-value.model';

export interface WorkerIdWithTotalsWithStatsInTime {
    workerId: string;
    totals: {
        total: TimedValue<number>[],
        average: TimedValue<number>[],
        min: TimedValue<number>[],
        max: TimedValue<number>[]
    };
}
