import { DurationTotalWithStats } from './duration-total-with-stats.model';

export interface WorkTotals {
    totalCompleted: number;
    totalInProgress: number;
    totalFailed: number;
    totalWaitingProcessing: number;
    duration: DurationTotalWithStats;
    clientDuration: DurationTotalWithStats;
    latency: DurationTotalWithStats;
}
