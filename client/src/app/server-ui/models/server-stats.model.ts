import { Duration } from './duration.model';
import { CircleWithPatchesDuration } from './circle-with-batches-duration.model';
import { WorkIdWithDuration } from './workid-with-duration.model';
import { WorkerIdWithTotals } from './worker-id-with-totals.model';
import { WorkerIdWithTotalsInTime } from './worker-id-with-totals-in-time.model';
import { WorkerIdTotalsWithStats } from './worker-id-totals-with-stats.model';
import { WorkerIdWithTotalsWithStatsInTime } from './worker-id-with-totals-with-stats-in-time.model';
import { WorkersAndWorkTotals } from './workers-and-work-totals.model';

export interface ServerStats {
    workGenerationStarted: boolean;
    workGenerationCompleted: boolean;
    workProcessingStarted: boolean;
    workProcessingCompleted: boolean;
    allDone: boolean;
    workGenerationDuration: Duration;
    totalDuration: Duration;
    workProcessingDuration: Duration;
    resultHandlingDuration: Duration;
    workAndWorkersTotals: WorkersAndWorkTotals;
    workGenerationsCirclesDurations: Duration[];
    generatedWorkFeedCirclesDurations: CircleWithPatchesDuration[];
    workCircleProcessingDurations: Duration[];
    individualWorkProcessingDurations: WorkIdWithDuration[];
    individualWorkProcessingTotalsPerWorker: WorkerIdWithTotals[];
    individualWorkProcessingTotalsPerWorkerInTime: WorkerIdWithTotalsInTime[];
    latencyByWorker: WorkerIdTotalsWithStats;
    latencyByWorkerInTime: WorkerIdWithTotalsWithStatsInTime[];
}
