export interface WorkerIdWithTotals {
    workerId: string;
    totals: {
        processing: number,
        completed: number,
        failed: number
    };
}
