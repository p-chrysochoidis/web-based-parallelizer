import { TimedValue } from './timed-value.model';

export interface WorkerIdWithTotalsInTime {
    workerId: string;
    totals: {
        processing: TimedValue<number>[],
        completed: TimedValue<number>[],
        failed: TimedValue<number>[]
    };
}
