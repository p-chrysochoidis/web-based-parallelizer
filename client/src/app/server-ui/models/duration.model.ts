export interface Duration {
    startedAt?: Date;
    endedAt?: Date;
    duration: number;
    otherSourceDuration?: number;
    durationsDiff?: number;
}
