export interface DurationTotalWithStatsFormated {
    total: string;
    average: string;
    min: string;
    max: string;
}
