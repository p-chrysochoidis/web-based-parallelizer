import { Duration } from './duration.model';

export interface CircleWithPatchesDuration {
    wholeCircle: Duration;
    batches: Duration[];
}
