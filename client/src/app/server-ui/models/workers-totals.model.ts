export interface WorkersTotals {
    totalConnected: number;
    totalProcesses: number;
    available: number;
    availableProcesses: number;
}
