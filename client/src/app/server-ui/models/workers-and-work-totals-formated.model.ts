import { WorkersTotals } from './workers-totals.model';
import { WorkTotalsFormated } from './work-totals-formated.model';

export interface WorkersAndWorkTotalsFormated {
    workers: WorkersTotals;
    work: WorkTotalsFormated;
}
