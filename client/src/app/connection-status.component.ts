import { Component } from '@angular/core';
import { SocketService } from './shared/socket.service';
import { BehaviorSubject } from 'rxjs';
import { faCircle, faSpinner } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-connection-status',
    templateUrl: './connection-status.component.html',
    styleUrls: ['./connection-status.component.scss']
})
export class ConnectionStatusComponent {
    conId: BehaviorSubject<string>;
    conStatus: BehaviorSubject<boolean>;
    faCircle = faCircle;
    faSpinner = faSpinner;

    constructor(private mSocketService: SocketService) {
      this.conId = this.mSocketService.connectionId;
      this.conStatus = this.mSocketService.connectionStatus;
    }
}
