import { Component, OnInit } from '@angular/core';
import { StatsService } from '../shared/stats.service';
import { map, debounceTime, auditTime, take, skip } from 'rxjs/operators';
import { Observable, merge } from 'rxjs';
import { WorkStats } from '../shared/models/work-stats.model';
import { WorkersAvailabilityChart } from './models/workers-availability-chart.model';
import { WorkProcessDurationChart } from './models/work-process-duration-chart.model';
import { WorkTotalsChart } from './models/work-totals-chart.model';
import { WorkOverTimeChart } from './models/work-over-time-chart.model';
import { WorkAverageOverTimeChart } from './models/work-average-over-time-chart.model';

@Component({
    templateUrl: './worker-ui.component.html',
    styleUrls: ['./worker-ui.component.scss']
})
export class WorkerUiComponent implements OnInit {
    public mTotalCpuCores: Observable<number>;
    public mTotalProcesses: Observable<number>;
    public mTotalWorkCompleted: Observable<number>;
    public mWorkersAvailabilityChartData: WorkersAvailabilityChart;
    public mWorkProcessDurationChartData: WorkProcessDurationChart;
    public mWorkTotalsChartData: WorkTotalsChart;
    public mWorkOverTimeChartData: WorkOverTimeChart;
    public mWorkProcessAverageDurationOverTimeChartData: WorkAverageOverTimeChart;
    private mStats: Observable<WorkStats>;
    private mStatsPerSec: Observable<WorkStats>;

    constructor(private mStatsService: StatsService) {
    }

    public ngOnInit() {
        this.mStats = this.mStatsService.stats.pipe(debounceTime(100));
        this.mStatsPerSec = this.getStatsMaxEmmitPerMillis(1000);
        this.mTotalCpuCores = this.mStats.pipe(map((stats) => stats.totalCpuCores));
        this.mTotalProcesses = this.mStats.pipe(map((stats) => stats.workers.total));
        this.mTotalWorkCompleted = this.mStats.pipe(map((stats) => stats.totalCompleted));
        this.mWorkersAvailabilityChartData = new WorkersAvailabilityChart(this.mStatsPerSec);
        this.mWorkProcessDurationChartData = new WorkProcessDurationChart(this.mStatsPerSec);
        this.mWorkTotalsChartData = new WorkTotalsChart(this.mStatsPerSec);
        this.mWorkOverTimeChartData = new WorkOverTimeChart(this.getStatsMaxEmmitPerMillis(5 * 1000));
        this.mWorkProcessAverageDurationOverTimeChartData = new WorkAverageOverTimeChart(this.getStatsMaxEmmitPerMillis(5 * 1000));
    }

    private getStatsMaxEmmitPerMillis(millis: number): Observable<WorkStats> {
        return merge(this.mStats.pipe(skip(1), auditTime(millis)), this.mStats.pipe(take(1)));
    }
}
