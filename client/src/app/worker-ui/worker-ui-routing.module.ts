import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkerUiComponent } from './worker-ui.component';

const workerUiRoutes: Routes = [
    { path: 'worker', component: WorkerUiComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(workerUiRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class WorkerUiRoutingModule { }
