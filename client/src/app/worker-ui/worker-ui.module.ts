import { NgModule } from '@angular/core';
import { WorkerUiRoutingModule } from './worker-ui-routing.module';
import { WorkerUiComponent } from './worker-ui.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppSharedModule } from '../shared/app.shared.module';

@NgModule({
    declarations: [
        WorkerUiComponent
    ],
    imports: [
        BrowserModule,
        WorkerUiRoutingModule,
        AppSharedModule
    ]
})
export class WorkerUiModule {}
