import { ChartDataModel } from '../../shared/models/chart-data.model';
import { Observable } from 'rxjs';
import { ChartData, ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { WorkStats } from '../../shared/models/work-stats.model';
import { map } from 'rxjs/operators';

export class WorkTotalsChart implements ChartDataModel {
    public chartData: Observable<ChartData>;
    public chartOptions: ChartOptions = this.createChartOptions();
    public chartType: ChartType = 'bar';
    private mLabels: string[] = ['Recieved', 'In Progress', 'Completed', 'Failed', 'Sent to server'];
    private readonly mDatasets: ChartDataSets[] = this.initDatasets();

    constructor(stats: Observable<WorkStats>) {
        this.chartData = this.initChartData(stats);
    }

    private initDatasets(): ChartDataSets[] {
        return [
            { data: [], backgroundColor: ['gray', 'orange', 'yellow', 'red', 'green'] }
        ];
    }

    private initChartData(statsStream: Observable<WorkStats>): Observable<ChartData> {
        return statsStream.pipe(
            map((stats: WorkStats) => {
                const data = this.mDatasets[0].data;
                data[0] = stats.totalRecieved;
                data[1] = stats.totalInProgress;
                data[2] = stats.totalCompleted;
                data[3] = stats.totalFailed;
                data[4] = stats.totalSentToServer;
                return {
                    labels: this.mLabels,
                    datasets: this.mDatasets,
                };
            })
        );
    }

    private createChartOptions() {
        return {
            responsive: false,
            title: { display: true, text: 'Work stats' },
            legend: { display: false },
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: { display: false }
                    }
                ]
            }
        };
    }
}
