import { ChartDataModel } from '../../shared/models/chart-data.model';
import { Observable } from 'rxjs';
import { ChartData, ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { WorkStats } from '../../shared/models/work-stats.model';
import { map } from 'rxjs/operators';
import { WorkDurationData } from '../../shared/models/work-duration-data.model';

export class WorkProcessDurationChart implements ChartDataModel {
    public chartData: Observable<ChartData>;
    public chartOptions: ChartOptions = this.createChartOptions();
    public chartType: ChartType = <any> 'scatter';
    private mLabels: string[] = [];
    private readonly mDatasets: ChartDataSets[] = this.initDatasets();

    constructor(stats: Observable<WorkStats>) {
        this.chartData = this.initChartData(stats);
    }

    private initDatasets(): ChartDataSets[] {
        return [
            { data: [], backgroundColor: '#808080', borderColor: 'gray' }
        ];
    }

    private initChartData(statsStream: Observable<WorkStats>): Observable<ChartData> {
        return statsStream.pipe(
            map((stats) => {
                return stats.workDurationsData;
            }),
            map((workDurationsData: Map<string, WorkDurationData>) => {
                const durations: WorkDurationData[] = [];
                workDurationsData.forEach((aDurationData) => durations.push(aDurationData));
                durations.filter((aDuration) => aDuration.processTime)
                    .sort((a, b) => a.timeRecieved - b.timeRecieved)
                    .forEach((aDuration, index) => {
                        this.mLabels[index] = `#${aDuration.workId}`;
                        this.mDatasets[0].data[index] = aDuration.processTime;
                    });
                return {
                    labels: this.mLabels,
                    datasets: this.mDatasets,
                };
            })
        );
    }

    private createChartOptions() {
        return {
            responsive: false,
            title: { display: true, text: 'Work process duration (ms)' },
            legend: { display: false },
            scales: {
                xAxes: [
                    {
                        type: 'category',
                        ticks: {
                            beginAtZero: true
                        }
                    }
                ],
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true
                        }
                    }
                ]
            },
        };
    }
}
