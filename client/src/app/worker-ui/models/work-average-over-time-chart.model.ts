import { ChartDataModel } from '../../shared/models/chart-data.model';
import { Observable } from 'rxjs';
import { ChartData, ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { WorkStats } from '../../shared/models/work-stats.model';
import { map } from 'rxjs/operators';
import { WorkDurationData } from '../../shared/models/work-duration-data.model';

export class WorkAverageOverTimeChart implements ChartDataModel {
    public chartData: Observable<ChartData>;
    public chartOptions: ChartOptions = this.createChartOptions();
    public chartType: ChartType = 'line';
    private readonly mDatasets: ChartDataSets[] = this.initDatasets();

    constructor(stats: Observable<WorkStats>) {
        this.chartData = this.initChartData(stats);
    }

    private initDatasets(): ChartDataSets[] {
        return [
            { label: 'Process duration average', data: [], backgroundColor: '#808080', borderColor: 'gray', fill: false }
        ];
    }

    private initChartData(statsStream: Observable<WorkStats>): Observable<ChartData> {
        return statsStream.pipe(
            map((stats: WorkStats) => stats.workDurationsData),
            map((workDurationData: Map<string, WorkDurationData>) => {
                const durations = [];
                workDurationData.forEach(aDuration => durations.push(aDuration));
                return durations;
            }),
            map((workDurations: WorkDurationData[]) => {
                // TODO: can be improved may don't start always from 0
                const data: any = this.mDatasets;
                let totalDuration = 0;
                let totalCompleted = 0;
                workDurations.filter((aDuration) => aDuration.timeSendToServer)
                    .sort((a, b) => a.timeSendToServer - b.timeSendToServer)
                    .forEach((aDuration, index) => {
                        totalDuration += aDuration.processTime;
                        totalCompleted++;
                        data[0].data[index] = {
                            t: new Date(aDuration.timeSendToServer),
                            y: Math.floor(totalDuration / totalCompleted)
                        };
                    });
                return {
                    labels: [],
                    datasets: this.mDatasets,
                };
            })
        );
    }

    private createChartOptions(): ChartOptions {
        return {
            responsive: false,
            title: { display: true, text: 'Work average process duration over time (ms)' },
            scales: {
                xAxes: [
                    { type: 'time', time: { unit: 'second' } }
                ],
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true
                        }
                    }
                ]
            },
        };
    }
}
