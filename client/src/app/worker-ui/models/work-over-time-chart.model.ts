import { ChartDataModel } from '../../shared/models/chart-data.model';
import { Observable } from 'rxjs';
import { ChartData, ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { WorkStats } from '../../shared/models/work-stats.model';
import { map } from 'rxjs/operators';
import { WorkDurationData } from '../../shared/models/work-duration-data.model';

export class WorkOverTimeChart implements ChartDataModel {
    public chartData: Observable<ChartData>;
    public chartOptions: ChartOptions = this.createChartOptions();
    public chartType: ChartType = 'line';
    private readonly mDatasets: ChartDataSets[] = this.initDatasets();

    constructor(stats: Observable<WorkStats>) {
        this.chartData = this.initChartData(stats);
    }

    private initDatasets(): ChartDataSets[] {
        return [
            { label: 'Recieved', data: [], backgroundColor: '#80808060', borderColor: 'gray' },
            { label: 'Completed', data: [], backgroundColor: '#ffff0060', borderColor: 'yellow' },
            { label: 'Failed', data: [], backgroundColor: '#ff000060', borderColor: 'red' }
        ];
    }

    private initChartData(statsStream: Observable<WorkStats>): Observable<ChartData> {
        return statsStream.pipe(
            map((stats: WorkStats) => stats.workDurationsData),
            map((workDurationData: Map<string, WorkDurationData>) => {
                const durations = [];
                workDurationData.forEach(aDuration => durations.push(aDuration));
                return durations;
            }),
            map((workDurations: WorkDurationData[]) => {
                // TODO: can be improved
                const data: any = this.mDatasets;
                let totalReceived = 0;
                let totalCompleted = 0;
                let totalFailed = 0;
                workDurations.filter((aDuration) => aDuration.timeRecieved)
                    .sort((a, b) => a.timeRecieved - b.timeRecieved)
                    .forEach((aDuration, index) => {
                        totalReceived++;
                        data[0].data[index] = {
                            t: new Date(aDuration.timeRecieved),
                            y: totalReceived
                        };
                    });
                workDurations.filter((aDuration) => aDuration.timeFinished && !aDuration.failed)
                    .sort((a, b) => a.timeFinished - b.timeFinished)
                    .forEach((aDuration, index) => {
                        totalCompleted++;
                        data[1].data[index] = {
                            t: new Date(aDuration.timeFinished),
                            y: totalCompleted
                        };
                    });
                workDurations.filter((aDuration) => aDuration.timeFinished && aDuration.failed)
                    .sort((a, b) => a.timeFinished - b.timeFinished)
                    .forEach((aDuration, index) => {
                        totalFailed++;
                        data[2].data[index] = {
                            t: new Date(aDuration.timeFinished),
                            y: totalFailed
                        };
                    });
                return {
                    labels: [],
                    datasets: this.mDatasets,
                };
            })
        );
    }

    private createChartOptions(): ChartOptions {
        return {
            responsive: false,
            title: { display: true, text: 'Work over time' },
            scales: {
                xAxes: [
                    { type: 'time', time: { unit: 'second' } }
                ],
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true
                        }
                    }
                ]
            }
        };
    }
}
