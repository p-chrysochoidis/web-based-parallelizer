import { ChartDataModel } from '../../shared/models/chart-data.model';
import { Observable } from 'rxjs';
import { ChartData, ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { WorkStats } from '../../shared/models/work-stats.model';
import { map } from 'rxjs/operators';

export class WorkersAvailabilityChart implements ChartDataModel {
    public chartData: Observable<ChartData>;
    public chartOptions: ChartOptions = this.createChartOptions();
    public chartType: ChartType = 'doughnut';
    private mLabels: string[] = ['Available Workers', 'Busy Workers'];
    private readonly mDatasets: ChartDataSets[] = this.initDatasets();

    constructor(stats: Observable<WorkStats>) {
        this.chartData = this.initChartData(stats);
    }

    private initDatasets(): ChartDataSets[] {
        return [
            { data: [], backgroundColor: ['green', 'red'] }
        ];
    }

    private initChartData(statsStream: Observable<WorkStats>): Observable<ChartData> {
        return statsStream.pipe(
            map((stats) => {
                return stats.workers;
            }),
            map((workersStats) => {
                const data = this.mDatasets[0].data;
                data[0] = workersStats.totalAvailable;
                data[1] = workersStats.totalWorking;
                return {
                    labels: this.mLabels,
                    datasets: this.mDatasets,
                };
            })
        );
    }

    private createChartOptions() {
        return {
            responsive: false,
            title: { display: true, text: 'Workers availability' }
        };
    }
}
