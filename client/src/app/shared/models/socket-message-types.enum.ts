export enum SocketMessageTypes {
    // incomming
    startWork = 'startWork',
    serverStats = 'serverStats',
    registerForServerStatsAnswer = 'registerForServerStatsAnswer',
    startWorkGenerationAnswer = 'startWorkGenerationAnswer',
    // outgoing
    register = 'register',
    workCompleted = 'workCompleted',
    registerForServerStats = 'registerForServerStats',
    startWorkGeneration = 'startWorkGeneration'
}
