import { WorkDurationData } from './work-duration-data.model';

export interface WorkStats {
    totalCpuCores: number;
    workers: {
        total: number;
        totalAvailable: number,
        totalWorking: number
    };
    totalRecieved: number;
    totalCompleted: number;
    totalFailed: number;
    totalInProgress: number;
    totalSentToServer: number;
    workDurationsData: Map<string, WorkDurationData>;
}
