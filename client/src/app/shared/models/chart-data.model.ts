import { Observable } from 'rxjs';
import { ChartData, ChartOptions, ChartType } from 'chart.js';

export interface ChartDataModel {
    chartData: Observable<ChartData>;
    chartOptions: ChartOptions;
    chartType: ChartType;
}
