import { WorkStatuses } from './work-statuses.enum';

export interface WorkDurationData {
    workId: string;
    timeRecieved: number;
    timeStarted: number;
    timeFinished: number;
    timeSendToServer: number;
    workStatus: WorkStatuses;
    processTime: number;
    failed: boolean;
}
