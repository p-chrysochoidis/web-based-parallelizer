import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faSquare } from '@fortawesome/free-regular-svg-icons';

@Component({
    selector: 'app-checkbox-badge',
    templateUrl: './checkbox-badge.component.html',
    styleUrls: ['./checkbox-badge.component.scss']
})
export class CheckboxBadgeComponent {
    @Input() label: string;
    @Input() state: Observable<boolean>;
    public faCheck = faCheck;
    public faSquare = faSquare;
}
