import { NgModule } from '@angular/core';
import { SocketService } from './socket.service';
import { WebWorkersService } from './web-workers.service';
import { StatsService } from './stats.service';
import { ChartComponent } from './chart.component';
import { BadgeComponent } from './badge.component';
import { CommonModule } from '@angular/common';
import { CheckboxBadgeComponent } from './checkbox-badge.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
    declarations: [
        ChartComponent,
        BadgeComponent,
        CheckboxBadgeComponent,
    ],
    providers: [
       SocketService,
       WebWorkersService,
       StatsService
    ],
    exports: [
        ChartComponent,
        BadgeComponent,
        CheckboxBadgeComponent
    ],
    imports: [
        CommonModule,
        FontAwesomeModule
    ]
})
export class AppSharedModule { }
