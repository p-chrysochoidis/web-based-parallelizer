import * as io from 'socket.io-client';
import { Injectable } from '@angular/core';
import * as uuidV4 from 'uuid/v4';
import { BehaviorSubject, Subject, Observable, race, of } from 'rxjs';
import { SocketMessageTypes } from './models/socket-message-types.enum';
import { environment } from '../../environments/environment';
import { filter, delay, map, switchMap, take } from 'rxjs/operators';

@Injectable()
export class SocketService {
    private readonly messageResponseTimeoutMillis: number = 5 * 1000;
    private mSocket: SocketIOClient.Socket;
    private mMessagesStream: Subject<any>;
    private mConnectionStatus: BehaviorSubject<boolean>;
    private mIsInitialized: boolean;
    private mConnectionId: BehaviorSubject<string>;

    constructor() {
        this.mMessagesStream = new Subject<any>();
        this.mConnectionStatus = new BehaviorSubject<boolean>(false);
        this.mConnectionId = new BehaviorSubject<string>(null);
    }

    public get connectionId(): BehaviorSubject<string> {
        return this.mConnectionId;
    }

    public get connectionStatus(): BehaviorSubject<boolean> {
        return this.mConnectionStatus;
    }

    public init() {
        if (this.mIsInitialized) {
            throw new Error('Socket connection already initialized');
        }
        this.mSocket = io(environment.wsServerLocation, { path: '/connect' });
        this.mSocket.on('connect', () => {
            this.mConnectionId.next(this.mSocket.id);
            this.mConnectionStatus.next(true);
        });
        this.mSocket.on('message', (msg) => {
            this.mMessagesStream.next(JSON.parse(msg));
        });
        this.mSocket.on('disconnect', () => {
            this.mConnectionId.next(null);
            this.mConnectionStatus.next(false);
        });
        this.mIsInitialized = true;
    }

    public get messages() {
        return this.mMessagesStream;
    }

    public registerForServerStats(): Observable<any> {
        return this.mConnectionStatus.pipe(
            filter((status) => status),
            switchMap(() => {
                return this.sendMessageWithResponse({
                    type: SocketMessageTypes.registerForServerStats
                })
                .pipe(map((responseMessage) => {
                    if (!responseMessage.success) {
                        throw new Error(`Register for server stats failed. ${responseMessage}`);
                    }
                    return responseMessage;
                }));
            })
        );
    }

    public sendRegisterMessage(numOfThreads: number) {
        this.sendMessage({
            type: SocketMessageTypes.register,
            numOfThreads: numOfThreads
        });
    }

    public sendStartWorkGenerationMessage(): Observable<any> {
        return this.sendMessageWithResponse({
            type: SocketMessageTypes.startWorkGeneration
        });
    }

    public sendWorkCompleted(workId: string, workResult: any, workProcessTime: number) {
        this.sendMessage({
            type: SocketMessageTypes.workCompleted,
            workId: workId,
            workResult: workResult,
            processTime: workProcessTime
        });
    }

    private generatedMessageId(): string {
        return uuidV4();
    }

    private sendMessage(jsonMessageWithoutId) {
        jsonMessageWithoutId.id = this.generatedMessageId();
        this.mSocket.send(JSON.stringify(jsonMessageWithoutId));
    }

    private sendMessageWithResponse(jsonMessageWithoutId): Observable<any> {
        this.sendMessage(jsonMessageWithoutId);
        return race(
            this.messages.pipe(filter((message) => message.replyForId === jsonMessageWithoutId.id)),
            of(null).pipe(
                delay(this.messageResponseTimeoutMillis),
                map(() => {
                    throw new Error(`Failed to recieve response in ${this.messageResponseTimeoutMillis}ms.`);
                })
            )
        ).pipe(take(1));
    }
}
