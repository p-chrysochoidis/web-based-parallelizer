import { Injectable } from '@angular/core';
import { WebWorkersService } from './web-workers.service';
import { BehaviorSubject } from 'rxjs';
import { WorkStats } from './models/work-stats.model';
import { WorkIds } from './models/workIds.model';
import { WorkStatuses } from './models/work-statuses.enum';
import { WorkDurationData } from './models/work-duration-data.model';

@Injectable()
export class StatsService {
    private mStats: BehaviorSubject<WorkStats> = new BehaviorSubject(this.getEmptyStats());
    constructor(private mWebWorkersService: WebWorkersService) {
    }

    public get stats() {
        return this.mStats;
    }

    public startRecording() {
        this.mStats.value.totalCpuCores = this.mWebWorkersService.totalCpuThreads;
        this.mStats.value.workers.total = this.mWebWorkersService.totalWorkers;
        this.mWebWorkersService.totalAvailableWorkers.subscribe((total) => {
            this.mStats.value.workers.totalAvailable = total;
            this.mStats.next(this.mStats.value);
        });
        this.mWebWorkersService.totalBusyWorkers.subscribe((total) => {
            this.mStats.value.workers.totalWorking = total;
            this.mStats.next(this.mStats.value);
        });
        this.mStats.next(this.mStats.value);
        this.mWebWorkersService.workStatusesUpdates.subscribe((workWithUpdate) => {
            this.updateWorkDuration(workWithUpdate);
        });
    }

    private updateWorkDuration(workWithUpdate: [WorkIds, WorkStatuses]) {
        const workIdKey = `${workWithUpdate[0].id}|${workWithUpdate[0].messageId}`;
        const workId = workWithUpdate[0].id;
        const status = workWithUpdate[1];
        const now = Date.now();
        const currentStats = this.mStats.value;
        const workDurationsDataMap = currentStats.workDurationsData;
        const workDurationData = workDurationsDataMap.get(workIdKey) || this.getEmptyWorkDuration(workId);
        workDurationData.workStatus = status;
        switch (status) {
            case WorkStatuses.received:
                workDurationData.timeRecieved = now;
                currentStats.totalRecieved++;
            break;
            case WorkStatuses.submited:
                workDurationData.timeStarted = now;
                currentStats.totalInProgress++;
            break;
            case WorkStatuses.completed:
                workDurationData.timeFinished = now;
                currentStats.totalInProgress--;
                currentStats.totalCompleted++;
            break;
            case WorkStatuses.failed:
                workDurationData.timeFinished = now;
                workDurationData.failed = true;
                currentStats.totalInProgress--;
                currentStats.totalFailed++;
            break;
            case WorkStatuses.sentToServer:
                workDurationData.timeSendToServer = now;
                if (workDurationData.timeRecieved) {
                    workDurationData.processTime = workDurationData.timeSendToServer - workDurationData.timeRecieved;
                }
                currentStats.totalSentToServer++;
            break;
        }
        workDurationsDataMap.set(workIdKey, workDurationData);
        this.mStats.next(this.mStats.value);
    }

    private getEmptyStats(): WorkStats {
        return {
            totalCpuCores: 0,
            workers: {
                total: 0,
                totalAvailable: 0,
                totalWorking: 0
            },
            totalRecieved: 0,
            totalCompleted: 0,
            totalFailed: 0,
            totalInProgress: 0,
            totalSentToServer: 0,
            workDurationsData: new Map()
        };
    }

    private getEmptyWorkDuration(workId: string): WorkDurationData {
        return {
            workId: workId,
            timeRecieved: null,
            timeStarted: null,
            timeFinished: null,
            timeSendToServer: null,
            workStatus: null,
            processTime: 0,
            failed: false,
        };
    }
}
