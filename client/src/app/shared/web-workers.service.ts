import { Injectable } from '@angular/core';
import { filter } from 'rxjs/operators';
import { SocketService } from './socket.service';
import { SocketMessageTypes } from './models/socket-message-types.enum';
import { Subject, BehaviorSubject } from 'rxjs';
import { WorkIds } from './models/workIds.model';
import { WorkStatuses } from './models/work-statuses.enum';

@Injectable()
export class WebWorkersService {
    private readonly MIN_WEB_WORKERS = 1;
    private readonly THREADS_TO_WEB_WORKERS_RATIO = 0.5;
    private mTotalThreads: number = navigator.hardwareConcurrency;
    private mIsInitialized: boolean = false;
    private mWebWorkers: Worker[] = [];
    private mAvailableWorkers: Worker[] = [];
    private mWorkerToWorkIdMap: Map<Worker, WorkIds> = new Map();
    private mWorkIdToWorkTimesMap: Map<string, number> = new Map();
    private readonly mWebWorkersToInititalize = this.calculateWebWorkersToInitialize();
    private mWorkStatusUpdateStream: Subject<[WorkIds, WorkStatuses]>;
    private mTotalAvailableWorkers: BehaviorSubject<number> = new BehaviorSubject(0);
    private mTotalBusyWorkers: BehaviorSubject<number> = new BehaviorSubject(0);

    constructor(private mSocketService: SocketService) {
        this.mWorkStatusUpdateStream = new Subject();
    }

    public get workStatusesUpdates (): Subject<[WorkIds, WorkStatuses]> {
        return this.mWorkStatusUpdateStream;
    }

    public get totalCpuThreads (): number {
        return this.mTotalThreads;
    }

    public get totalWorkers (): number {
        return this.mWebWorkersToInititalize;
    }

    public get totalAvailableWorkers (): BehaviorSubject<number> {
        return this.mTotalAvailableWorkers;
    }

    public get totalBusyWorkers (): BehaviorSubject<number> {
        return this.mTotalBusyWorkers;
    }

    public init(): void {
        if (this.mIsInitialized) {
            throw new Error('Service is already initialized.');
        }
        this.handleSocketConnectionStatus();
        this.handleSocketMessages();
        this.mIsInitialized = true;
    }

    private getWorkProcessTime(workId: string): number {
        const startTime = this.mWorkIdToWorkTimesMap.get(workId);
        if (startTime) {
            return Date.now() - startTime;
        }
        return null;
    }

    private initializeWebWorkers() {
        this.mWebWorkers = [];
        this.mAvailableWorkers = [];
        for (let i = 0; i < this.mWebWorkersToInititalize; i++) {
            const aWorker = new Worker('/api/webworker.js');
            this.mWebWorkers.push(aWorker);
            this.handleMessagesFromWebWorker(aWorker, i);
            this.mAvailableWorkers.push(aWorker);
        }
        this.notifyForWorkersAvailabilityNumbers();
    }

    private calculateWebWorkersToInitialize() {
        const total = Math.floor(this.mTotalThreads * this.THREADS_TO_WEB_WORKERS_RATIO);
        return Math.max(this.MIN_WEB_WORKERS, total);
    }

    private handleMessagesFromWebWorker(aWorker: Worker, index: number) {
        aWorker.onmessage = (workerMessage) => {
            const workResult = workerMessage.data;
            const workIds = this.mWorkerToWorkIdMap.get(aWorker);
            this.mWorkerToWorkIdMap.delete(aWorker);
            this.mAvailableWorkers.push(aWorker);
            this.notifyForWorkersAvailabilityNumbers();
            this.updateWorkStatus(workIds, WorkStatuses.completed);
            this.mSocketService.sendWorkCompleted(workIds.id, workResult.data, this.getWorkProcessTime(workIds.id));
            this.mWorkIdToWorkTimesMap.delete(workIds.id);
            this.updateWorkStatus(workIds, WorkStatuses.sentToServer);
        };
    }

    private handleSocketMessages() {
        this.mSocketService.messages
        .pipe(filter((message) => {
            return message.type === SocketMessageTypes.startWork;
        })).subscribe((message) => {
            this.updateWorkStatus(this.getWorkIdsFromWorkMessage(message), WorkStatuses.received);
            this.sendWorkToAvailableWorker(message);
        });
    }

    private handleSocketConnectionStatus() {
        this.mSocketService.connectionStatus.subscribe(
            (connected) => {
                if (connected) {
                    this.initializeWebWorkers();
                    this.mSocketService.sendRegisterMessage(this.mWebWorkersToInititalize);
                } else {
                    this.mWebWorkers.forEach((aWorker) => {
                        if (this.mWorkerToWorkIdMap.has(aWorker)) {
                            const workIds = this.mWorkerToWorkIdMap.get(aWorker);
                            this.updateWorkStatus(workIds, WorkStatuses.failed);
                            this.mWorkerToWorkIdMap.delete(aWorker);
                            this.mWorkIdToWorkTimesMap.delete(workIds.id);
                        }
                        aWorker.terminate();
                    });
                    this.mWebWorkers = [];
                    this.mAvailableWorkers = [];
                    this.notifyForWorkersAvailabilityNumbers();
                }
            }
        );
    }

    private notifyForWorkersAvailabilityNumbers() {
        this.mTotalAvailableWorkers.next(this.mAvailableWorkers.length);
        this.mTotalBusyWorkers.next(this.mWebWorkers.length - this.mAvailableWorkers.length);
    }

    private sendWorkToAvailableWorker(workMessage: any) {
        const aWorker = this.mAvailableWorkers.shift();
        const workIds = this.getWorkIdsFromWorkMessage(workMessage);
        const work = workMessage.work;
        this.mWorkIdToWorkTimesMap.set(workIds.id, Date.now());
        if (aWorker) {
            this.notifyForWorkersAvailabilityNumbers();
            this.updateWorkStatus(workIds, WorkStatuses.submited);
            this.mWorkerToWorkIdMap.set(aWorker, workIds);
            aWorker.postMessage(work);
        }
    }

    private updateWorkStatus(workIds: WorkIds, status: WorkStatuses) {
        this.mWorkStatusUpdateStream.next([workIds, status]);
    }

    private getWorkIdsFromWorkMessage(workMessage: any): WorkIds {
        return {
            id: workMessage.work.id,
            messageId: workMessage.messageId
        };
    }
}
