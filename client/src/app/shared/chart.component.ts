import { Component, Input, ViewChild, ElementRef, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ChartData, ChartType, ChartOptions } from 'chart.js';
import * as Chart from 'chart.js';
import { Observable, Subscription } from 'rxjs';

@Component({
    selector: 'app-chart',
    templateUrl: './chart.component.html',
    styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() public type: ChartType;
    @Input() public data: Observable<ChartData>;
    @Input() public options: ChartOptions;
    @ViewChild('chart') private mChartCanvas: ElementRef;
    private mChart: Chart;
    private mChartData: ChartData;
    private mDataSubscription: Subscription;

    public ngAfterViewInit() {
        this.initChart();
    }

    public ngOnDestroy() {
        if (this.mDataSubscription) {
            this.mDataSubscription.unsubscribe();
            this.mDataSubscription = null;
        }
    }

    public ngOnInit() {
        this.mDataSubscription = this.data.subscribe((chartData) => {
            this.mChartData = chartData;
            if (this.mChart) {
                this.mChart.data.labels = chartData.labels;
                this.mChart.data.datasets = chartData.datasets;
                this.mChart.update();
            } else {
                this.initChart();
            }
        });
    }

    private initChart() {
        if (this.mChartCanvas) {
            if (!this.mChart) {
                this.mChart = new Chart(
                    this.mChartCanvas.nativeElement,
                    {
                        type: this.type,
                        data: this.mChartData,
                        options: this.options
                    }
                );
            }
        }
    }
}
