import { Component, OnInit } from '@angular/core';
import { SocketService } from './shared/socket.service';
import { WebWorkersService } from './shared/web-workers.service';
import { StatsService } from './shared/stats.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'web-base-parallelizer-client';

  constructor(
    private mSocketService: SocketService,
    private mWebWorkersService: WebWorkersService,
    private mStatsService: StatsService) {
  }

  ngOnInit() {
    this.mStatsService.startRecording();
    this.mWebWorkersService.init();
    this.mSocketService.init();
  }
}
