Nowadays the computing power is plentiful, due to the rapid development of the
computing sector and the widespread use of computers in the form of various devices
such as desktops, laptops, smartphones, tablets and gaming machines. At the same time,
with the development of the IT sector, the development of Internet applications has also
been remarkable. As a result, such applications are a big part of the global application
development and trend to rise.
For the present thesis, based on the above observations, we develop an
application that allows distributed processing using web technologies. The use of these
technologies is chosen in order to help developers solve the problems they are interested
in by writing their code using the JavaScript programming language. This choice was
also made because these apps can run on almost all devices that support web browsers.
After the development and presentation of the application, we are testing the
application using two problems. One is the calculation of π using the Monte Carlo
method and the other one is counting the words of a large file Word Count. These tests
were quite satisfactory and showed that the use of these technologies, including Node.js
and Web Workers, can offer solutions in the field of distributed processing, but they also
present many different challenges.

Keywords: Distributed Computing, Node.js, Web Workers, JavaScript,
Volunteer Computing